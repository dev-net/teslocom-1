<?php

define('DIR_SLIDERS', DIR_PUBLICDATA_SC.'/sliders');
define('URL_SLIDERS', URL_PRODUCTS_PICTURES.'/../sliders');

class slidersActions extends ActionsController {
  
  /**
   * ACTION
   * Save action trigger
   */
  function save_action(){
    $aid = intval($this->getData('aid'));
    
    $data = $this->getData();
    $errors = array();

    if (!$errors = SLDRcheckDataAction($data)) { 
      if (SLDRissetAction($aid)){
        SLDRupdateAction($aid, $data); 
      }else {
        $aid = SLDRnewAction($data);
      }
    }
    
    if (!empty($errors)){
       Message::raiseMessageRedirectSQ(MSG_ERROR, 'ukey=sliders', 'Не все поля заполнены');
    }
    else
      Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders&aid='.$aid, 'msg_information_save');
  }

  /**
   * ACTION
   * Save mailing trigger
   */
  function save_mailing(){
    $mid = intval($this->getData('mid'));
    
    $data = $this->getData();
    $errors = array();

    if (!$errors = SLDRcheckDataMailing($data)) { 
      if (SLDRissetMailing($mid)){
        SLDRupdateMailing($mid, $data);
      }else {
        $mid = SLDRnewMailing($data);
      }
    }
    
    if (!empty($errors)){
       Message::raiseMessageRedirectSQ(MSG_ERROR, 'ukey=sliders', 'Не все поля заполнены');
    }
    else
      Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders&mid='.$mid.$str, 'msg_information_save');
  }
  
  /**
   * ACTION
   * Remove actions
   */
  function remove_action(){
    $data = $this->getData();
    $errors = array();
    
    if (isset($data['delete_field'])){
      if (!empty($data['delete_field'])){
        foreach ($data['delete_field'] as $id){
          SLDRremoveAction($id);
        }
      }
    }
    
    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders', 'msg_information_save');
  }

  /**
   * ACTION
   * Remove mailings
   */
  function remove_mailing(){
    $data = $this->getData();
    $errors = array();
    
    if (isset($data['delete_field'])){
      if (!empty($data['delete_field'])){
        foreach ($data['delete_field'] as $id){
          SLDRremoveMailing($id);
        }
      }
    }
    
    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders', 'msg_information_save');
  }
  
  /**
   * ACTION
   * Ulpoad image.
   */
  function save_images(){
    
    $data = $this->getData();        
    $error = SLDRuploadImage($data);
    
    if (!empty($error))
      Message::raiseMessageRedirectSQ(MSG_ERROR, 'ukey=sliders&aid='.$data['aid'], $error);
    else
      Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders&aid='.$data['aid'], 'msg_information_save');
  }
  
  /**
   * ACTION
   * do send mailing
   */
  function send_mailing() {
    $data = $this->getData();
    
    $mailing = SLDRgetMailing($data['mid']);
        
    if ($data['who'] == 'me'){

      ss_mail(CONF_GENERAL_EMAIL, $mailing['title'], $mailing['text_ru'], true);
      Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders&mid='.$data['mid'], 'Рассылка произведена успешно только мне');
    }
    elseif ($data['who'] == 'all'){
      
      $emails = SLDRgetEmails();
      foreach ($emails as $email) {
        ss_mail($email, $mailing['title'], $mailing['text_ru'], true);  
      }
      
      Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=sliders&mid='.$data['mid'], 'Рассылка произведена успешно всем');
    }    
  }

  /**
   * Main trigger
   */
  function main() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    /* @var $smarty Smarty */
    $GetVars = &$Register->get(VAR_GET);
    
    // error 
    $errors = isset($GetVars['errors'])? $GetVars['errors']: '';    
    if (!empty($errors)){
      $smarty->assign('errors',$errors);
    }

    $smarty->assign('URL_SLIDERS',URL_SLIDERS);
    
    // load action if exsist
    $aid = isset($GetVars['aid'])? intval($GetVars['aid']): 0;
    if ($action = SLDRissetAction($aid)){
      $smarty->assign('action', $action);
      
      $product = SLDRgetProduct($action['product_code']);
      $smarty->assign('product', $product);
      
      //Product images       
      $images = SLDRgetImages($aid);      
      foreach ($images as $_ind=>$_val){
        if ( file_exists(DIR_SLIDERS.'/'.$images[$_ind]['filename']) && trim($images[$_ind]['filename']) != '' ){
          $images[$_ind]['image_exists'] = 1;
          list($images[$_ind]['image_width'], $images[$_ind]['image_height']) = getimagesize(DIR_SLIDERS.'/'.$images[$_ind]['filename']);
          $images[$_ind]['large_image'] = array('size' => sprintf('%0.0f KB',round(filesize(DIR_SLIDERS.'/'.$images[$_ind]['filename'])/1024)),'file' => $images[$_ind]['filename'], 'width' => $images[$_ind]['image_width'], 'height' => $images[$_ind]['image_height']);
        }else{
          $images[$_ind]['large_image'] = array('size' => 0,'file' => $images[$_ind]['filename'], 'width' => 0, 'height' => 0);
        }
        if ( file_exists(DIR_SLIDERS.'/'.$images[$_ind]['enlarged']) && trim($images[$_ind]['enlarged']) != '' ){
          $images[$_ind]['enlarged_exists'] = 1;
          list($images[$_ind]['enlarged_width'], $images[$_ind]['enlarged_height']) = getimagesize(DIR_SLIDERS.'/'.$images[$_ind]['enlarged']);
          $images[$_ind]['enlarged_image'] = array('size' => sprintf('%0.0f KB',round(filesize(DIR_SLIDERS.'/'.$images[$_ind]['enlarged'])/1024)),'file' => $images[$_ind]['enlarged'], 'width' => $images[$_ind]['enlarged_width'], 'height' => $images[$_ind]['enlarged_height']);
        }elseif($images[$_ind]['image_exists'] == 1){
          $images[$_ind]['enlarged_exists'] = 1;
          $images[$_ind]['enlarged_image'] = $images[$_ind]['large_image'];
        }else{
          $images[$_ind]['enlarged_image'] = array('size' =>0,'file' => $images[$_ind]['enlarged'], 'width' => 0, 'height' => 0);
        }
  
        if ( file_exists(DIR_SLIDERS.'/'.$images[$_ind]['thumbnail'])&& trim($images[$_ind]['thumbnail']) != '' ){
          $images[$_ind]['thumbnail_exists'] = 1;
          list($images[$_ind]['thumbnail_width'], $images[$_ind]['thumbnail_height']) = getimagesize(DIR_SLIDERS.'/'.$images[$_ind]['thumbnail']);
          $images[$_ind]['thumbnail_image'] = array('size' => sprintf('%0.0f KB',round(filesize(DIR_SLIDERS.'/'.$images[$_ind]['thumbnail'])/1024)),'file' => $images[$_ind]['thumbnail'], 'width' => $images[$_ind]['thumbnail_width'], 'height' => $images[$_ind]['thumbnail_height']);
        }else{
          $images[$_ind]['thumbnail_image'] = array('size' =>0,'file' => $images[$_ind]['thumbnail'], 'width' => 0, 'height' => 0);
        }
  
      }
      $smarty->assign('images',$images);
    }
    
    // load mailing if exsist
    $mid = isset($GetVars['mid'])? intval($GetVars['mid']): 0;
    if ($mailing = SLDRissetMailing($mid)){
      $smarty->assign('mailing',$mailing);
    }
    
    $actions = SLDRgetActions();
    $smarty->assign('actions',$actions);
    
    $mailings = SLDRgetMailings();
    $smarty->assign('mailings',$mailings);
    
    $smarty->assign('admin_sub_dpt', 'sliders.html');
  }
}  
  ActionsController::exec('slidersActions');  
?>