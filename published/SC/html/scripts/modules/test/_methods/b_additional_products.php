<?php

class additinalProductsActions extends ActionsController {


  public function save_action() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);
    $PostVars = &$Register->get(VAR_POST);

    $enabled = isset($PostVars['enabled']) ? $PostVars['enabled'] : 0;
    db_phquery("UPDATE ?#TBL_CONFIG_SETTINGS SET SettingValue = ? WHERE SettingName = 'additional_products'", $enabled);

    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'did=235', 'msg_information_save');
  }

  public function save_prefix() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);
    $PostVars = &$Register->get(VAR_POST);

    $cid = isset($PostVars['cid']) ? (int) $PostVars['cid'] : 0;
    $prefix = isset($PostVars['prefix']) ? $PostVars['prefix'] : '';

    $this->setPrefix($cid, $prefix);

    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=additional_products&action=iFRAMEprefix&cid='.$cid, 'msg_information_save');
  }

  public function save_sufix() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);
    $PostVars = &$Register->get(VAR_POST);

    $cid = isset($PostVars['cid']) ? (int) $PostVars['cid'] : 0;
    $sufix = isset($PostVars['sufix']) ? $PostVars['sufix'] : '';

    $this->setSufix($cid, $sufix);

    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=additional_products&action=iFRAMEsufix&cid='.$cid, 'msg_information_save');
  }

  public function save_sub_product() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);
    $PostVars = &$Register->get(VAR_POST);

    $iid = isset($PostVars['iid']) ? (int) $PostVars['iid'] : 0;

    $pid = isset($PostVars['pid']) ? (int) $PostVars['pid'] : 0;
    $item = isset($PostVars['item']) ? trim(mysql_real_escape_string($PostVars['item'])) : '';
    $name = isset($PostVars['name_custom']) ? trim(mysql_real_escape_string($PostVars['name_custom'])) : '';
    $noindex = isset($PostVars['noindex_custom']) ? (int) $PostVars['noindex_custom'] : 0;

    $this->updateSubProductCustom($pid, $item, $name, $noindex);

    Message::raiseMessageRedirectSQ(MSG_SUCCESS, 'ukey=additional_products&action=iFRAMEsub&iid='.$iid, 'msg_information_save');
  }

  /**
   * @param $pid
   * @param $item
   * @param $name
   * @param $noindex
   */
  private function updateSubProductCustom($pid, $item, $name, $noindex){
    if ($this->getSubProductCustom($pid, $item)){
      db_phquery("UPDATE ?#PRODUCTS_SUB_CUSTOM SET name=?, noindex=? WHERE pid=? AND item=? ", $name, $noindex, $pid, $item);
    } else {
      db_query("INSERT INTO ".PRODUCTS_SUB_CUSTOM." (`pid`, `item`, `name`, `noindex`) VALUES ('{$pid}', '{$item}', '{$name}', '{$noindex}')");
    }
  }


  /**
   * iFrame modal
   */
  public function iFRAMEsufix() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);

    $cid = (int) $GetVars['cid'];

    $sufix = $this->getSufix($cid);
    $smarty->assign('sufix', $sufix);
    $smarty->assign('cid', $cid);
    $smarty->assign('admin_sub_dpt', 'additional_products_sufix.html');
  }

  /**
   * iFrame modal
   */
  public function iFRAMEprefix() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);

    $cid = (int) $GetVars['cid'];

    $prefix = $this->getPrefix($cid);
    $smarty->assign('prefix', $prefix);
    $smarty->assign('cid', $cid);
    $smarty->assign('admin_sub_dpt', 'additional_products_prefix.html');
  }

  /**
   * @param int $cid
   * @param int $offset
   */
  public function iFRAMEsubsList($cid = 0, $offset = 0) {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);

    $per_page   = 20;
    if (empty($cid))
      $cid = (int) $GetVars['cid'];

    if (empty($offset))
      $offset = (isset($GetVars['offset']) ? intval($GetVars['offset']) : 0);

    $count = $this->getSubCount($cid);
    if ( isset($offset) ) {
      $offset = (int)$offset;
    } else { $offset = 0; }

    $offset -= $offset % $per_page;
    if ( $offset < 0 ) $offset = 0;

    $subs       = $this->getSubProductsFromCid($offset, $per_page, $cid);
    $navigator  = $this->getNavigator($count, $offset, $per_page);

    $smarty->assign('subs', $subs);
    $smarty->assign('navigator', $navigator);
    $smarty->assign('cid', $cid);
    $smarty->assign('admin_sub_dpt', 'additional_products_list.html');
  }

  /**
   *
   */
  public function iFRAMEsub() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);

    $iid = (int) $GetVars['iid'];

    $sub_product        = $this->getSubProduct($iid);
    $sub_product_custom = $this->getSubProductCustom($sub_product['pid'], $sub_product['item']);

    $sub_product['name'] = stripslashes($sub_product['name']);
    $sub_product['item'] = stripslashes($sub_product['item']);
    $sub_product_custom['name'] = stripslashes($sub_product_custom['name']);
    $smarty->assign('sub_product', $sub_product);
    $smarty->assign('sub_product_custom', $sub_product_custom);
    $smarty->assign('admin_sub_dpt', 'additional_product_custom.html');
  }

  /**
   * @param $iid
   * @return array|bool
   */
  public function getSubProduct($iid) {
    $q = db_query("SELECT pi.*, ps.*, p.*, psc.name AS name_custom, psc.noindex AS noindex_custom FROM ".PRODUCTS_ITEMS." AS pi
                    LEFT JOIN ".PRODUCTS_SUB_CUSTOM." AS psc ON (pi.pid = psc.pid AND pi.item = psc.item)
                    JOIN ".PRODUCTS_SUB." AS ps ON pi.iid = ps.iid
                    JOIN ".PRODUCTS_TABLE." AS p ON p.productID = pi.pid
                    WHERE pi.iid = '{$iid}'") or die (db_error());
    $r = db_fetch_assoc($q);
    if (!empty($r)) {
      return $r;
    } else
      return false;
  }

  /**
   * @param $pid
   * @param $item
   * @return array|bool
   */
  public function getSubProductCustom($pid, $item) {
    $q = db_query("SELECT * FROM ".PRODUCTS_SUB_CUSTOM." WHERE pid = '{$pid}' AND item = '{$item}'") or die (db_error());
    $r = db_fetch_assoc($q);
    if (!empty($r)) {
      return $r;
    } else
      return false;
  }

  /**
   * @param int $count
   * @param int $offset
   * @param int $per_page
   * @return mixed
   */
  public function getNavigator($count = 0, $offset = 0, $per_page = 20) {
    ShowNavigator($count, $offset, $per_page, '', $out);
    return $out;
  }

  /**
   * @param int $cid
   * @return mixed
   */
  private function getSubCount($cid = 0) {
    $where = '';
    if (!empty($cid)) {
      $where = 'WHERE p.categoryID = '.$cid;
    }

    $q = db_query("SELECT count(*) as c, pi.*, p.* FROM ".PRODUCTS_SUB."  as sp
                     JOIN ".PRODUCTS_ITEMS." as pi ON pi.iid=sp.iid
                     JOIN ".PRODUCTS_TABLE." as p ON p.productID=pi.pid
                     {$where}");
    $p = db_fetch_assoc($q);
    return $p['c'];
  }


  /**
   * @param int $offset
   * @param int $per_page
   * @param int $cid
   * @return array
   */
  private function getSubProductsFromCid ($offset = 0, $per_page = 20, $cid = 0) {
    if (empty($offset)) {
      $page = 1;
    } else {
      $page = intval($offset/$per_page) + 1;
    }
    $start = ($page - 1) * $per_page;
    $limit = "LIMIT {$start}, {$per_page}";

    $result = array();
    $q = db_query("SELECT pi.*, ps.*, p.categoryID, p.productID, psc.name AS name_custom, psc.noindex AS noindex_custom FROM ".PRODUCTS_ITEMS." AS pi
                    LEFT JOIN ".PRODUCTS_SUB_CUSTOM." AS psc ON (pi.pid = psc.pid AND pi.item = psc.item)
                    JOIN ".PRODUCTS_SUB." AS ps ON pi.iid = ps.iid
                    JOIN ".PRODUCTS_TABLE." AS p ON p.productID = pi.pid
                    WHERE p.categoryID = '{$cid}' ORDER BY ps.name ASC {$limit}");
    while ($p = db_fetch_assoc($q)) {
      $result[] = $p;
    }
    return $result;
  }

  /**
   * AJAX Get count of products
   */
  public function AJAXcountProducts() {
    $ids = trim(strip_tags($_GET['ids']));

    $q = db_query("SELECT count(*) as c FROM ".PRODUCTS_TABLE." p
                    JOIN ".CATEGORIES_TABLE." as c ON p.categoryID=c.categoryID
                    WHERE (p.categoryID IN ({$ids}))");
    $r = db_fetch_assoc($q);

    if (!empty($r)) {
      echo json_encode(array('do' => 'success', 'count' => $r['c']));
    } else {
      echo json_encode(array('do' => 'error', 'msg' => "Не удалось получить кол-во продуктов."));
    }
    die;
  }

  /**
   * AJAX Get count of subproducts
   */
  public function AJAXcountSubProducts() {
    $q = db_query("SELECT count(*) as c FROM ((SELECT count(*) as c FROM ".PRODUCTS_SUB." GROUP BY name HAVING c > 1)  as `temp`)");
    $r = db_fetch_assoc($q);

    if (!empty($r)) {
      echo json_encode(array('do' => 'success', 'count' => $r['c']));
    } else {
      echo json_encode(array('do' => 'error', 'msg' => "Не удалось получить кол-во дубликатов."));
    }
    die;
  }

  /**
   * AJAX Create Items per step.
   */
  public function AJAXreItemsPerStep() {
    $step = intval($_GET['step']);
    $per_step = intval($_GET['per_step']);
    $ids = trim(strip_tags($_GET['ids']));

    if ($this->createItems($ids, $step, $per_step)) {
      echo json_encode(array('do' => 'success'));
    } else {
      echo json_encode(array('do' => 'error', 'msg' => "Не удалось создать список подтоваров."));
    }
    die;
  }

  /**
   * AJAX Create subProducts per ste.
   */
  public function AJAXreSubsPerStep() {
    $step = intval($_GET['step']);
    $per_step = intval($_GET['per_step']);
    $ids = trim(strip_tags($_GET['ids']));

    if ($this->createSubs($ids, $step, $per_step)) {
      echo json_encode(array('do' => 'success'));
    } else {
      echo json_encode(array('do' => 'error', 'msg' => "Не удалось создать список подтоваров."));
    }
    die;
  }

  /**
   * AJAX Update NoIndex
   */
  public function AJAXreNoIndexPerStep() {
    $step = intval($_GET['step']);
    $per_step = intval($_GET['per_step']);

    if ($this->updateNoIndex($step, $per_step)) {
      echo json_encode(array('do' => 'success'));
    } else {
      echo json_encode(array('do' => 'error', 'msg' => "Не удалось создать список подтоваров."));
    }
    die;
  }

  /**
   * AJAX Clear noindex param
   */
  public function AJAXclearNoIndex() {
    db_phquery("UPDATE ?#PRODUCTS_SUB SET noindex=0 WHERE 1 ");

    echo json_encode(array('do' => 'success'));
    die;
  }

  /**
   * AJSX Clear items
   */
  public function AJAXclearItems() {
    $this->_clearItems();

    echo json_encode(array('do' => 'success'));
    die;
  }

  /**
   * AJAX Clear subproducts.
   */
  public function AJAXclearSubs() {
    $this->_clearSubs();
    $this->_clearCounts(0);

    echo json_encode(array('do' => 'success'));
    die;
  }

  /**
   * AJAX Set counts.
   */
  public function AJAXsetCountSubProducts() {
    $q = db_query("SELECT count(*) as c, p.categoryID as cid, p.*, pi.*, ps.* FROM ".PRODUCTS_SUB." as ps
                          JOIN ".PRODUCTS_ITEMS." as pi ON pi.iid=ps.iid
                          JOIN ".PRODUCTS_TABLE." as p ON p.productID=pi.pid
                          GROUP BY p.categoryID");
    while ($r = db_fetch_assoc($q)) {
      db_phquery("UPDATE ?#CATEGORIES_TABLE SET subproducts_count = ? WHERE categoryID = ?", $r['c'], $r['cid']);
    }
    $this->setSubCounts();

    echo json_encode(array('do' => 'success'));
    die;
  }

  /**
   *
   */
  private function setSubCounts() {
    $q = db_query("SELECT subproducts_count, parent, categoryID FROM ".CATEGORIES_TABLE." ORDER BY categoryID DESC");
    while ($r = db_fetch_assoc($q)) {
      db_phquery("UPDATE ?#CATEGORIES_TABLE SET subproducts_count = subproducts_count + ? WHERE categoryID = ?", $r['subproducts_count'], $r['parent']);
    }
  }

  /**
   * @param $cid
   * @return bool
   */
  private function getPrefix($cid) {
    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID = '{$cid}' ") or die (db_error());
    $r = db_fetch_assoc($q);
    if (!empty($r)) {
      return $r['prefix'];
    } else
      return false;
  }

  /**
   * @param $cid
   * @param $prefix
   */
  private function setPrefix($cid, $prefix) {
    db_phquery("UPDATE ?#CATEGORIES_TABLE SET prefix=? WHERE categoryID=?", $prefix, $cid);
  }

  /**
   * @param $cid
   * @return bool
   */
  private function getSufix($cid) {
    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID = '{$cid}' ") or die (db_error());
    $r = db_fetch_assoc($q);
    if (!empty($r)) {
      return $r['sufix'];
    } else
      return false;
  }

  /**
   * @param $cid
   * @param $sufix
   */
  private function setSufix($cid, $sufix) {
    db_phquery("UPDATE ?#CATEGORIES_TABLE SET sufix=? WHERE categoryID=?", $sufix, $cid);
  }

  /**
   * @param $cids
   * @param int $step
   * @param int $per_step
   * @return bool
   */
  private function createItems($cids, $step = 0, $per_step = 5) {
    $where_limit = " LIMIT {$step}, {$per_step}";

    $q = db_query("SELECT * FROM ".PRODUCTS_TABLE." p
                    JOIN ".CATEGORIES_TABLE." as c ON p.categoryID=c.categoryID
                    WHERE (p.categoryID IN ({$cids})) ORDER BY p.productID ASC {$where_limit}");

    while ($p = db_fetch_assoc($q)){
      $items = prdGetItems($p['brief_description_ru']);

      if (!empty($items)) {
        prdCreateItems($p['productID'], $items);
      }
    }
    return true;
  }

  /**
   * @param $cids
   * @param int $step
   * @param int $per_step
   * @return bool
   */
  private function createSubs($cids, $step = 0, $per_step = 5) {
    $where_limit = " LIMIT {$step}, {$per_step}";

    $q = db_query("SELECT * FROM ".PRODUCTS_TABLE." p
                    JOIN ".CATEGORIES_TABLE." as c ON p.categoryID=c.categoryID
                    WHERE (p.categoryID IN ({$cids})) ORDER BY p.productID ASC {$where_limit}");

    while ($p = db_fetch_assoc($q)){
      $items = $this->getItems($p['productID']);
      if (!empty($items)) {
        prdCreateSubs($items, array('prefix' => $p['prefix'], 'sufix' => $p['sufix'], 'features_ru' => $p['features_ru']));
      }
    }
    return true;
  }

  /**
   * @param $pid
   * @return array|bool
   */
  private function getItems($pid) {
    $q = db_query("SELECT * FROM ".PRODUCTS_ITEMS." WHERE pid = '{$pid}'");
    $items = array();
    while ($item = db_fetch_assoc($q)) {
      $items[] = $item;
    }
    return $items;
  }



  /**
   * @param int $step
   * @param int $per_step
   * @return bool
   */
  private function updateNoIndex($step = 0, $per_step = 10) {
    $where_limit = " LIMIT {$step}, {$per_step}";

    if (db_phquery("UPDATE ".PRODUCTS_SUB." SET noindex=1 WHERE name in (
                  SELECT name FROM (
                    SELECT name FROM ".PRODUCTS_SUB." GROUP BY name HAVING count(*) > 1 {$where_limit}) as `temp`
                  )")) {
      return true;
    };
      return false;
  }

  /**
   *
   */
  public function models_action() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $PostVars = &$Register->get(VAR_POST);

    $text = $PostVars['models'];
    $smarty->assign('old_models', $text);

    $pid = 3165;
    $items = prdCompatibility($text, 0, 0, 0, 0, 1);
    $text = prdCompatibility($text, $pid, $items, 1, 1);

    $smarty->assign('models', $text);
    $smarty->assign('admin_sub_dpt', 'additional_products.html');
  }

  /**
   * @param $counts
   */
  private function updateCounts($counts) {
    foreach ($counts as $cid => $count) {
      db_phquery("UPDATE ?#CATEGORIES_TABLE SET subproducts_count=? WHERE categoryID=?", $count, $cid);
    }
  }

  /**
   *
   */
  public function _clearCounts() {
    db_phquery("UPDATE ?#CATEGORIES_TABLE SET subproducts_count=0 WHERE categoryID <> 0");
  }

  /**
   *
   */
  public function _clearSubs() {
    db_query("DELETE FROM ".PRODUCTS_SUB." WHERE 1");
    db_query("ALTER TABLE ".PRODUCTS_SUB." AUTO_INCREMENT = 1");
  }

  /**
   *
   */
  public function _clearItems() {
    db_query("DELETE FROM ".PRODUCTS_ITEMS." WHERE 1");
    db_query("ALTER TABLE ".PRODUCTS_ITEMS." AUTO_INCREMENT = 1");
  }

  /**
   * Get list of categories.
   * @return array.
   */
  private function getCategories()
  {
    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID<>0 ") or die (db_error());
    $categories = array();
    while ($category = db_fetch_assoc($q)) {
      $categories[] = $category;
    }

    return $categories;
  }

  /**
   * @param $categories
   * @param int $root
   * @return string
   */
  private function modifyTreeData($categories, $root = 1){
    $result = "";
    foreach ($categories as $category){
      if ($category['parent'] == $root){
        $name = rawurldecode ($category['name_ru']. " (Товаров ".$category['products_count']." / Подтоваров ".$category['subproducts_count'].")");
        $sufix = !empty($category['sufix']) ? $category['sufix'] : 'не указан.';
        $prefix = !empty($category['prefix']) ? $category['prefix'] : 'не указан.';

        $children = $this->modifyTreeData($categories, $category['categoryID']);
        $result .= '<li data-id="'.$category['categoryID'].'">'.$name."<span>(Префикс: {$prefix})</span><span>(Суфикс: {$sufix})</span>".$children.'</li>';
      }
    }
    if (!empty($result))
      $result = "<ul>{$result}</ul>";
    return $result;
  }

  /**
   * @return mixed
   */
  public function checkSetting() {
    $q = db_query("SELECT * FROM ".TBL_CONFIG_SETTINGS." WHERE SettingName = 'additional_products' ") or die (db_error());
    $s = db_fetch_assoc($q);
    return $s['SettingValue'];
  }

  /**
   * Main trigger
   */
  public function main() {
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    $GetVars = &$Register->get(VAR_GET);
    /* @var $smarty Smarty */

    if (isset($GetVars['offset'])) {
      $this->iFRAMEsubsList($GetVars['cid'], $GetVars['offset']);

    } else {
      $enabled = $this->checkSetting();
      $categories = $this->getCategories();
      $categories = $this->modifyTreeData($categories);

      $smarty->assign('enabled', $enabled);
      $smarty->assign('categories', $categories);
      $smarty->assign('admin_sub_dpt', 'additional_products.html');
    }
  }

}

ActionsController::exec('additinalProductsActions');

?>