<?php
class Inflow extends Module {

    function initInterfaces(){

        $this->Interfaces = array(
            'inflow' => array(
                'key' => 'inflow',
                'name' => 'Inflow',
                'method' => 'methodInflow',
            ),
        );
    }

    function methodInflow(){

        global $smarty;
        include(DIR_MODULES.'/'.$this->ModuleDir.'/scripts/inflow.php');
    }
}
?>