<?php
	define('ROBOXCHANGE_TTL', 'ROBOXchange');
	define('ROBOXCHANGE_DSCR', 'Интеграция с платежной системой ROBOXchange (www.roboxchange.com) с сервисом Cash Register.<br />Интеграция реализована по описанию, представленному по адресу http://www.roboxchange.com/Environment/CashRegister/ru/index.aspx');
	
	define('ROBOXCHANGE_CFG_LANG_TTL', 'Язык интерфейса');
	define('ROBOXCHANGE_CFG_LANG_DSCR', 'Выберите язык интерфейса на сервере ROBOXchange, который увидит покупатель при оплате');
	
	define('ROBOXCHANGE_TXT_LANGRU', 'Русский');
	define('ROBOXCHANGE_TXT_LANGEN', 'Английский');
	
	define('ROBOXCHANGE_CFG_MERCHANTLOGIN_TTL', 'Login магазина в обменном пункте');
	define('ROBOXCHANGE_CFG_MERCHANTLOGIN_DSCR', 'Информация о Вашем аккаунте продавца в платежной системе ROBOXchange');
	
	define('ROBOXCHANGE_CFG_ROBOXCURRENCY_TTL', 'Выберите валюту обменного пункта, в которой будет происходить оплата клиентом');
	define('ROBOXCHANGE_CFG_ROBOXCURRENCY_DSCR', '');
	
	define('ROBOXCHANGE_CFG_SHOPCURRENCY_TTL', 'Выберите валюту магазина, которой соответсвует выбранная Вами валюта обменного пункта');
	define('ROBOXCHANGE_CFG_SHOPCURRENCY_DSCR', 'Выберите из списка валют Вашего интернет-магазина');
	
	define('ROBOXCHANGE_CFG_MERCHANTPASS1_TTL', 'Пароль №1');
	define('ROBOXCHANGE_CFG_MERCHANTPASS1_DSCR', '');
	
	define('ROBOXCHANGE_TXT_NOCURR', 'ОШИБКА: Не удалось получить список валют с сервера ROBOXchange');
	
	define('ROBOXCHANGE_TXT_PROCESS', 'Оплатить заказ сейчас!');
?>