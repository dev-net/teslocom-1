<?php
/**
 * @connect_module_class_name ROBOXchange
 * @package DynamicModules
 * @subpackage Payment
 */
	class ROBOXchange extends PaymentModule {

		var $type = PAYMTD_TYPE_ONLINE;
		var $language = 'rus';
		
		function _initVars(){
			
			parent::_initVars();
			$this->title = ROBOXCHANGE_TTL;
			$this->description = ROBOXCHANGE_DSCR;
			$this->sort_order = 1;
			
			$this->Settings = array( 
					'CONF_ROBOXCHANGE_MERCHANTLOGIN',
					'CONF_ROBOXCHANGE_MERCHANTPASS1',
					'CONF_ROBOXCHANGE_LANG',
					'CONF_ROBOXCHANGE_ROBOXCURRENCY',
					'CONF_ROBOXCHANGE_SHOPCURRENCY',
				);
		}
	
		function _initSettingFields(){
	
			$this->SettingsFields['CONF_ROBOXCHANGE_MERCHANTLOGIN'] = array(
				'settings_value' 		=> '', 
				'settings_title' 			=> ROBOXCHANGE_CFG_MERCHANTLOGIN_TTL,
				'settings_description' 	=> ROBOXCHANGE_CFG_MERCHANTLOGIN_DSCR,
				'settings_html_function' 	=> 'setting_TEXT_BOX(0,', 
				'sort_order' 			=> 1,
			);
			$this->SettingsFields['CONF_ROBOXCHANGE_MERCHANTPASS1'] = array(
				'settings_value' 		=> '', 
				'settings_title' 			=> ROBOXCHANGE_CFG_MERCHANTPASS1_TTL, 
				'settings_description' 	=> ROBOXCHANGE_CFG_MERCHANTPASS1_DSCR, 
				'settings_html_function' 	=> 'setting_TEXT_BOX(0,', 
				'sort_order' 			=> 1,
			);
			$this->SettingsFields['CONF_ROBOXCHANGE_LANG'] = array(
				'settings_value' 		=> '', 
				'settings_title' 			=> ROBOXCHANGE_CFG_LANG_TTL,
				'settings_description' 	=> ROBOXCHANGE_CFG_LANG_DSCR,
				'settings_html_function' 	=> 'setting_SELECT_BOX(ROBOXchange::_getLanguages(),', 
				'sort_order' 			=> 1,
			);
			$this->SettingsFields['CONF_ROBOXCHANGE_ROBOXCURRENCY'] = array(
				'settings_value' 		=> '', 
				'settings_title' 			=> ROBOXCHANGE_CFG_ROBOXCURRENCY_TTL, 
				'settings_description' 	=> ROBOXCHANGE_CFG_ROBOXCURRENCY_DSCR, 
				'settings_html_function' 	=> 'setting_SELECT_BOX(ROBOXchange::_getRoboxCurrencies(),', 
				'sort_order' 			=> 1,
			);
			$this->SettingsFields['CONF_ROBOXCHANGE_SHOPCURRENCY'] = array(
				'settings_value' 		=> '', 
				'settings_title' 			=> ROBOXCHANGE_CFG_SHOPCURRENCY_TTL, 
				'settings_description' 	=> ROBOXCHANGE_CFG_SHOPCURRENCY_DSCR, 
				'settings_html_function' 	=> 'setting_CURRENCY_SELECT(', 
				'sort_order' 			=> 1,
			);
		}
	
		function after_processing_html( $orderID ){
		
			$res = '';
			
			$order = ordGetOrder( $orderID );
			
			$order_amount = RoundFloatValue(PaymentModule::_convertCurrency($order['order_amount'],0,$this->_getSettingValue('CONF_ROBOXCHANGE_SHOPCURRENCY')));

			$post_1 = array(
				'mrh' => $this->_getSettingValue('CONF_ROBOXCHANGE_MERCHANTLOGIN'),
				'out_summ' => $order_amount,
				'inv_id' => $orderID,
			);
			
			$post_1['crc'] = md5(implode(':',$post_1).':'.$this->_getSettingValue('CONF_ROBOXCHANGE_MERCHANTPASS1'));
			
			$post_1['lang'] = $this->_getSettingValue('CONF_ROBOXCHANGE_LANG');
			$post_1['in_curr'] = $this->_getSettingValue('CONF_ROBOXCHANGE_ROBOXCURRENCY');
			$post_1['inv_desc'] = CONF_SHOP_NAME;
			
      $hidden_fields_html = '';
      reset($post_1);
      
      while(list($k,$v)=each($post_1)){
      	
				$hidden_fields_html .= '<input type="hidden" name="'.xHtmlSpecialChars($k).'" value="'.xHtmlSpecialChars($v).'" />'."\n";
      }
      
			$res = '
				<form method="post" action="https://www.roboxchange.com/ssl/calc.asp" style="text-align:center;">
					'.$hidden_fields_html.'
					<input type="submit" value="'.xHtmlSpecialChars(ROBOXCHANGE_TXT_PROCESS).'" />
				</form>
				';
			return $res;
		}
	
		function _getLanguages(){
			return ROBOXCHANGE_TXT_LANGRU.':ru,'.ROBOXCHANGE_TXT_LANGEN.':en';
		}
		
		function _getRoboxCurrencies(){
			/*
			http://www.roboxchange.com/xml/currlist.asp
			*/
			$options = array();
			
			$error_options = $options;
			
			$error_options[] = array(
				'title' => ROBOXCHANGE_TXT_NOCURR,
				'value' => ''
				);
			
			$ch=curl_init();
			if(!$ch)return $error_options;
			
			curl_setopt ($ch, CURLOPT_URL,'http://www.roboxchange.com/xml/currlist.asp');
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			initCurlProxySettings($ch);
			
			$http_response = curl_exec($ch);
			
			if(!$http_response)return $error_options;
			curl_close($ch);

			$xmlNode = new xmlNodeX();
			$xmlNode->renderTreeFromInner($http_response);
			$r_xmlItem = $xmlNode->getChildrenByName('item');
			foreach ($r_xmlItem as $xmlItem){
				/* @var $xmlItem xmlNodeX */
				list($xmlCurrName) = $xmlItem->getChildrenByName('curr_name');
				list($xmlCurr) = $xmlItem->getChildrenByName('curr');
				$options[] = array(
					'title' => $xmlCurrName->getData(),
					'value' => $xmlCurr->getData(),
					);
			}
			
			return $options;
		}
	}
?>