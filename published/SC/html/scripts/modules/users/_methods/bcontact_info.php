<?php

class EditCustomerContactInfo extends ActionsController
{
	function save_contact_info()
	{
	    $ci = $this->getData('ci');

      if(!array_key_exists('subscribed4news', $ci))
      {
          $ci['subscribed4news'] = 0;
      };
      
      $ce = new Customer();
      $ce->loadByID($_GET['userID']);
      
      $ci['ActivationCode'] = ($ci['activated'] == '1' ? '' : ($ce->ActivationCode != '' ? $ce->ActivationCode : substr(md5(time()),mt_rand(0,15),16) ));
      unset($ci['activated']);
      
      Users::updateCustomerInfo($ce->customerID, $ci);
      
      RedirectSQ();
	}
  
  function save_partner(){
    
    safeMode(true);
    
    $data = $this->getData();    
    PRTNRsave($data);
    
    $nwps= scanArrayKeysForID($data, array( "nwp" ) );
    $uid = intval($data['customerID']);
    
    if (!empty($uid)) {
      foreach( $nwps as $key => $val ){                
        PRTNRdelNWP($key, $uid);
        
        $nwp = 0;
        if ( isset($val["nwp"]) )
          $nwp = intval($val["nwp"]);
          
        if (!empty($nwp)){
          PRTNRaddNWP($key, $uid, $nwp);
        }
      }
    }
    
    //RedirectSQ();
    Message::raiseMessageRedirectSQ(MSG_SUCCESS, '', 'msg_information_save'); 
  }
	
    function main()
    {
        $smarty = &Core::getSmarty();
        Users::_initUserInfo();
        set_query('safemode=','',true);

        $customerEntry = new Customer;
        $customerEntry->loadByID($_GET['userID']);
        
        $customer_groups = GetAllCustGroups();
        
        $cust_group_name = '-';
        foreach($customer_groups as $group_info)
        {
            if($group_info['custgroupID'] == $customerEntry->custgroupID)
            {
                $cust_group_name = $group_info['custgroup_name'];
            };
        };

        $reg_fields = GetRegFields();
        $cust_reg_fields = GetRegFieldsValuesByCustomerID($customerEntry->customerID);

        foreach($reg_fields as $key => $reg_fld)
        {
            $f = false;
            foreach($cust_reg_fields as $cfld)
            {
                if($cfld['reg_field_ID'] == $reg_fld['reg_field_ID'])
                {
                    $reg_fields[$key]['reg_field_value'] = $cfld['reg_field_value'];
                    $f = true;
                    break;
                };
            };
            if(!$f) $reg_fields[$key]['reg_field_value'] = '';
        };
        
        $smarty->assign('eLink', array('title' => translate('btn_edit'), 'href' => 'javascript: void(0);', 'onclick' => 'showEditForm();'));
        
        $smarty->assign('customer_groups', $customer_groups);
        $smarty->assign('cust_group_name', $cust_group_name);
        $smarty->assign('reg_fields', $reg_fields);
        $smarty->assign('customerInfo', $customerEntry->getVars());
        
        $nwps = PRTNRgetNWPs($_GET['userID']);
        $categories = $this->getCategories();
        $categories = $this->modifyTreeData($categories, $nwps);
        $smarty->assign('categories', $categories);
        
        $managers = PRTNRgetManagers();
        $smarty->assign('managers', $managers);

        $manager_default = PRTNRgetManagerDefault();
        $smarty->assign('manager_default', $manager_default);
        
        $smarty->assign('UserInfoFile', 'backend/user_contact.html');    
    }

  /**
   * Get list of categories.
   * @return array. 
   */
  private function getCategories()
  {
    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID<>0 ") or die (db_error());
    $categories = array();
    while ($category = db_fetch_assoc($q)) {
      $categories[] = $category;
    }  

    return $categories;
  }
  
  /**
   * Generate HTML tree.
   * @param $categories - data from db.
   * @param $nwps - num of w_prices
   * @param $root - level nodes.
   * @return HTML tree.
   */
  private function modifyTreeData($categories, $nwps, $root = 1){
    $result = ""; 
    foreach ($categories as $category){
      if ($category['parent'] == $root){
        $name = rawurldecode ($category['name_ru']);

        $nw_price = '';
        foreach($nwps as $nwp){
          if ($nwp['cid'] == $category['categoryID'])
            $nw_price = $nwp['nw_price'];
        }

        $children = $this->modifyTreeData($categories, $nwps, $category['categoryID']);
        $result .= "<li>{$name}<input type='text' name='nwp_{$category['categoryID']}' value='{$nw_price}'> {$children} </li>";
      }
    }
    if (!empty($result))
      $result = "<ul>{$result}</ul>";
    return $result;
  }
  
};

ActionsController::exec('EditCustomerContactInfo');

?>