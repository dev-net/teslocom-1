<?php

class priceWholesaleController extends ActionsController {
	
	public function main()
	{
		$Register = &Register::getInstance();
		$GetVars = &$Register->get(VAR_GET);
		$PostVars = &$Register->get(VAR_POST);
		$smarty = &$Register->get(VAR_SMARTY);
		
    if (!empty($PostVars)) {
      $smarty->assign('success',1);
      
      global $smarty_mail;
      $smarty_mail->assign( "company", trim(htmlspecialchars(strip_tags(addslashes($PostVars['company'])))) );
      $smarty_mail->assign( "email", trim(htmlspecialchars(strip_tags(addslashes($PostVars['email'])))) );
      $smarty_mail->assign( "phone", trim(htmlspecialchars(strip_tags(addslashes($PostVars['phone'])))) );
      $smarty_mail->assign( "models", trim(htmlspecialchars(strip_tags(addslashes($PostVars['models'])))) );
      
      $html = $smarty_mail->fetch( "price_wholesale.txt" );
            
      ss_mail(CONF_GENERAL_EMAIL, translate("email_subject_registration"), $html, true);
    }
	}

}

ActionsController::exec('priceWholesaleController');