<?php
class getPriceList extends Module {
  private $type = 0;
  private $nwp = 0;

  function initInterfaces(){

    $this->Interfaces = array(
      'getpricelist' => array(
        'key' => 'getpricelist',
        'name' => 'getPriceList',
        'method' => 'methodGetPriceList',
      ),
    );
  }

  function methodGetPriceList(){

    global $smarty;
    include(DIR_MODULES.'/'.$this->ModuleDir.'/scripts/getpricelist.php');
  }

  /**
   * @param int $type
   * @param int $nwp
   * @return array|bool
   */
  function getData($type = 1, $nwp = 0){
    $this->nwp = $nwp;
    $this->type = $type;

    $data = $this->getDatas();
    if (!empty($data)) {
      return $data;
    }

    return false;
  }

  /**
   * @return array
   */
  private function getDatas(){
    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID<>0 ORDER BY sort_order ASC") or die (db_error());
    $categories = array();
    $datas = array();
    while ($category = db_fetch_assoc($q)) {
      $categories[] = $category;
    }

    if (!empty($categories)) {
      $categories = $this->categoriesSort($categories);
      $datas = $this->generateData($categories);
    }
    return $datas;
  }

  /**
   * @param $categories
   * @return array
   */
  private function generateData($categories){
    $result = array();

    foreach ($categories as $category){
      if ($category['lvl'] == 1)
        $result[] = array('categoryID' => $category['categoryID'], 'name_ru' => $category['name_ru'], 'lvl' => $category['lvl']);

      $products = $this->getProducts($category['categoryID'], $category['lvl']);
      if (!empty($products)) {
        if ($category['lvl'] != 1)
          $result[] = array('categoryID' => $category['categoryID'], 'name_ru' => $category['name_ru'], 'count' => count($products), 'lvl' => $category['lvl']);
        $result = array_merge($result, $products);
      }
    }

    return $result;
  }

  /**
   * @param $categories
   * @param int $root
   * @param int $i
   * @return array
   */
  private function categoriesSort($categories, $root = 1, $i = 0){
    $result = array();
    $i++;
    foreach ($categories as $category){
      if ($category['parent'] == $root){

        $result[] = array('categoryID' => $category['categoryID'], 'name_ru' => $category['name_ru'], 'lvl' => $i);
        $children = $this->categoriesSort($categories, $category['categoryID'], $i);
        if (!empty($children)) {
          $result = array_merge($result, $children);
        }
      }
    }
    return $result;
  }

  /**
   * @param $cid
   * @param $lvl
   * @return array
   */
  private function getProducts($cid, $lvl){
    $where = "";
    if ($this->type == 2) {
      $where = " AND product_code LIKE '__-__-____'";
    }

    $q = db_query("SELECT * FROM ".PRODUCTS_TABLE." WHERE categoryID = '{$cid}' AND enabled = 1 AND in_stock > 0 {$where} ORDER BY sort_order ASC, name_ru ASC") or die (db_error());
    $products = array();

    $prtnr = 0;
    if (isset($_SESSION['is_partner'])){
      if (isset($_SESSION['choose_p'])){
        $prtnr = PRTNRgetFromId(intval($_SESSION['choose_p']));
      }
      else {
        $prtnr = PRTNRget();
      }
      $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID']);
    }

    while ($product = db_fetch_assoc($q)) {
      if (!empty( $this->nwp)) {
        $product = _PRTNRgeneratePrice($product, $prtnr, 0, $this->nwp);
      } else {
        //$product = PRTNRgeneratePrice($product);
      }

      $products[] = array('name_ru'   => $product['name_ru'],
                          'price'     => $product['Price'],
                          'code'      => $product['product_code'],
                          'in_stock'  => $product['in_stock'],
                          'productID' => $product['productID'],
                          'slug'      => $product['slug'],
                          'lvl'       => $lvl);
    }
    return $products;
  }
}
?>