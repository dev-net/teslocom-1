<?php
define('CARTVIEW_FADE', 'fade');
define('CARTVIEW_FRAME', 'frame');
define('CARTVIEW_WIDGET', 'widget');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

class getPriceListController extends ActionsController{

  /**
   * @param int $type
   * @param int $nwp
   * @param int $save
   */
  function generatePriceList($type = 1, $nwp = 0, $save = 0) {

    $class = new getPriceList;
    $datas = $class->getData($type, $nwp);

    include_once(DIR_CLASSES.'/PHPExcel.php');
//		include_once(DIR_CLASSES.'/PHPExcel/IOFactory.php');

    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_memcache;
    $cacheSettings = array( 'memcacheServer' => 'localhost', 'memcachePort' => 11211, 'cacheTime' => 600 );
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

    $objPHPExcel = PHPExcel_IOFactory::load(DIR_PUBLICDATA_SC.'/prices/pricelist.xls');

    $i = 7;
    $n = array();
    $space = "";
    $prev_root = 1;
    $min_root = 4;

    $styleProduct = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
      ),
      'font' => array(
        'size' => 12,
      ),
      'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
      ),
    );

    $manager_text = "";
    if ($manager = PRTNRyourManager())
      $manager_text = "{$manager['first_name']} {$manager['last_name']} Тел.: {$manager['Phone']} E-mail: {$manager['Email']}";

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()
      ->setCellValue('A3', $manager_text)->mergeCells("A3:C3");
    $objPHPExcel->getActiveSheet()->getStyle("A3:C3")->getFont()->setBold(true)->setSize(10);

    $objPHPExcel->getActiveSheet()
      ->setCellValue('A4', "Оптовый отдел Тел.: +7 (495) 786-80-13  E-mail: teslocom@mail.ru")->mergeCells("A4:C4");
    $objPHPExcel->getActiveSheet()->getStyle("A4:C4")->getFont()->setBold(true)->setSize(10);

    foreach ($datas as $data) {

      //set_time_limit(20);

      if (isset($data['code'])) {
        $objPHPExcel->getActiveSheet()
          ->setCellValue('A'.$i, $data['code'])
          ->setCellValue('B'.$i, $data['name_ru'])
          ->setCellValue('C'.$i, $data['price']);

        $site = "http://teslocom.ru/";
        if (!empty($data['slug'])) {
          $product_url = "/?ukey=product&productID={$data['productID']}&product_slug={$data['slug']}";
        } else {
          $product_url = "/?ukey=product&productID={$data['productID']}";
        }

        $objPHPExcel->getActiveSheet()->getCell('A' . $i)
          ->getHyperlink($data['code'])
          ->setUrl($site.$product_url);

        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setOutlineLevel($data['lvl']);
        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setVisible(false);
        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setCollapsed(true);

        $i++;
      } else {
        if ($prev_root < $data['lvl'])
          $n[$data['lvl']] = 0;

        $n[$data['lvl']]++;
        switch ($data['lvl']) {
          case 1: $space = "";	      break;
          case 2:	$space = "  ";	    break;
          case 3:	$space = "    ";	  break;
          case 4:	$space = "      ";	break;
        }

        if ($data['lvl'] < $min_root) {
          $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, $space."{$n[$data['lvl']]}. ".$data['name_ru'])->mergeCells("A{$i}:C{$i}");
          $objPHPExcel->getActiveSheet()->getStyle("A{$i}:C{$i}")->getFont()->setBold(true)->setItalic(true);//->setSize(14 - $data['lvl']);

          if ($data['lvl'] == 1)
            $objPHPExcel->getActiveSheet()->getStyle("A{$i}:C{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

          $prev_root = $data['lvl'];
          $i++;
        }
      }
    }

    $objPHPExcel->setActiveSheetIndex(0)->getStyle("A7:C{$i}")->applyFromArray($styleProduct);

    if (!$save) {

      //set_time_limit(30);

      if ($type == 1)
        $filename = "Общее наличие";
      elseif ($type == 2)
        $filename = "Наличие на Филях";

      if (!empty($nwp)){
        switch ($nwp) {
          case 1: $wp = "партнёра";	  break;
          case 2:	$wp = "оптовика";	  break;
          case 3:	$wp = "ресселера";	break;
          case 4:	$wp = "диллера";		break;
        }
        $filename .= " (с колонкой цен для {$wp})";
      }
      $date = date('d-m-Y', time());
      $filename .= " ".$date;

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header("Content-Disposition: attachment;filename='{$filename}.xls'");
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
      exit;
    } else {
      $filename = "t{$type}nwp{$nwp}";

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save(DIR_PUBLICDATA_SC."/prices/{$filename}.xls");
    }

    $objPHPExcel->disconnectWorksheets();
    unset($objPHPExcel);
  }

  /**
   * @param $all
   * @param $type
   * @param $nwp
   */
  function generateCronPriceList($all, $type, $nwp) {
    if ($all) {
      for ($type = 1; $type < 3; $type++)
        for ($nwp = 1; $nwp < 5; $nwp++)
          $this->generatePriceList($type, $nwp, 1);

    } else {
      $this->generatePriceList($type, $nwp, 1);
    }
  }

  /**
   * @param $type
   * @param $nwp
   */
  function getPriceList($type, $nwp){
    if (file_exists(DIR_PUBLICDATA_SC."/prices/t{$type}nwp{$nwp}.xls")) {
//			include_once(DIR_CLASSES.'/PHPExcel.php');
      include_once(DIR_CLASSES.'/PHPExcel/IOFactory.php');
/*
      $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite3;
      PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
*/
      $objReader = PHPExcel_IOFactory::createReader('Excel5');
      $objPHPExcel = $objReader->load(DIR_PUBLICDATA_SC."/prices/t{$type}nwp{$nwp}.xls");

      if ($type == 1)
        $filename = "Общее наличие";
      elseif ($type == 2)
        $filename = "Наличие на Филях";

      if (!empty($nwp)){
        switch ($nwp) {
          case 1: $wp = "партнёра";	  break;
          case 2:	$wp = "оптовика";	  break;
          case 3:	$wp = "ресселера";	break;
          case 4:	$wp = "диллера";		break;
        }
        $filename .= " (для {$wp})";
      }
      $date = date('d-m-Y', time());
      $filename .= " ".$date;

      $objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header("Content-Disposition: attachment;filename='{$filename}.xls'");
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');

      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
      $objWriter->save('php://output');
      exit;
    }
  }

  /**
   *
   */
  function main(){
    $Register = &Register::getInstance();
    $smarty = &$Register->get(VAR_SMARTY);
    /*@var $smarty Smarty*/
    $GetVars = &$Register->get(VAR_GET);

    $type = (isset($GetVars['t'])) ? intval($GetVars['t']) : 1;

    $nwp = 0;
    if (isset($GetVars['nwp'])) {
      if (PRTNRisManager()) {
        $nwp = intval($GetVars['nwp']);
      } else {
        if ($partner = PRTNRget())
          $nwp = PRTNRgetNWPrice($partner);
      }
    }

    if (isset($GetVars['cron'])){
      if (isset($GetVars['all'])){
        $this->generateCronPriceList(1);
      } else {
        $this->generateCronPriceList(0, $type, $nwp);
      }
      exit;
    }

    if (isset($GetVars['get'])){
      $this->getPriceList($type, $nwp);
      exit;
    }

    $this->generatePriceList($type, $nwp);
    exit;
  }
}

ActionsController::exec('getPriceListController');
?>