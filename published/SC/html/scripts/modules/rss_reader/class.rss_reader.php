<?php

class rss_reader extends ComponentModule {

	public function initInterfaces()
	{
		$this->__registerComponent('rss_reader','Ридер RSS ленты',array(TPLID_GENERAL_LAYOUT, TPLID_HOMEPAGE),'methodRssReader',array(
			'count' => array(
				'type' => 'text',
				'params' => array(
					'name' => 'count',
					'title' => 'Количество записей',
					'value' => 4
				)
			),
			'url' => array(
				'type' => 'text',
				'params' => array(
					'name' => 'url',
					'title' => 'URL rss-канала',
					'value' => ''
				)
			),
			'date_format' => array(
				'type' => 'text',
				'params' => array(
					'name' => 'date_format',
					'title' => 'Формат даты',
					'value' => 'Y-m-d H:i'
				)
			),
		));
	}
	
	public function methodRssReader($call_settings = null)
	{
		$local_settings = ( isset($call_settings['local_settings']) ) ? $call_settings['local_settings'] : array();
		$count = isset( $local_settings['count'] ) ? $local_settings['count'] : 4;
		$url = $local_settings['url'];
		$date_format = isset( $local_settings['date_format'] ) ? $local_settings['date_format'] : 'Y-m-d H:i';
		
		if ( empty($url) )
		{
			$file_name = DIR_DATA_SC.'/rss.xml';
			if ( file_exists($file_name) )
				$url = $file_name;
		}
		
		$rss = simplexml_load_file($url);
		
		if ( $rss )
		{
			$channel = array();
			$channel['title'] = (string) $rss->channel->title;
			$channel['link'] = (string) $rss->channel->link;
			//$dateTime = new DateTime( (string) $rss->channel->lastBuildDate );
			//$channel['date'] = $dateTime->format($date_format);
			$channel['date'] = date($date_format, strtotime( (string) $rss->channel->lastBuildDate ) );
			
			
			$items = array();
			$k = 0;
			foreach ( $rss->channel->item as $item )
			{
				$title =  (string) $item->title;
				$link = (string) $item->link;
				if ( empty($title) || empty($link) )
					continue;
				$items[$k]['title'] = $title;
				$items[$k]['link'] = $link;
				//$dateTime = new DateTime( (string) $item->pubDate );
				//$items[$k++]['date'] = $dateTime->format('Y-m-d H:i');
				$items[$k++]['date'] = date($date_format, strtotime( (string) $item->pubDate ) );
				
				if ( $k >= $count )
					break;
			}
			$channel['items'] = $items;
		}
		
		$Register = &Register::getInstance();
		$smarty = &$Register->get(VAR_SMARTY);
		$smarty->assign('channel',$channel);
		echo $smarty->fetch('rss_reader.html');
	}

}