-
@features "SS Premium product"
@state begin
-

	+---------------------+
	| Shop-Script PREMIUM |
	+---------------------+

Thank you for choosing Shop-Script PREMIUM!
-
@features "SS Premium product"
@state end
-
-
@features "SS Pro product"
@state begin
-

	+---------------------+
	| Shop-Script PRO     |
	+---------------------+

Thank you for choosing Shop-Script PRO!
-
@features "SS Pro product"
@state end
-

Before using this software please refer to installation/upgrade
and usage instructions which can be downloaded from Shop-Script website:
http://www.shop-script.com

----
Copyright (c) WebAsyst LLC, 2005. All rights reserved.
Shop-Script homepage:
http://www.shop-script.com