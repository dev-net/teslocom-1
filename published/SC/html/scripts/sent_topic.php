<?		function discAddDiscussion2( $productID, $Author, $Topic, $Body, $email){
	
			$sql = '
				INSERT ?#DISCUSSIONS_TABLE (productID, Author, Body, add_time, Topic, email) VALUES(?,?,?,?,?,?)
			';
			db_phquery($sql,$productID,$Author,$Body,Time::dateTime(),$Topic, $email);
			discUpdateRSSFeed($productID);
		}
		$smarty = new Smarty();
		//echo SMARTY_DIR;

		$customer_name = trim(htmlspecialchars(strip_tags($_POST["nick"])));
		$customer_email = trim(htmlspecialchars(strip_tags($_POST["email_disc"]))) ;
		$message_subject = trim(htmlspecialchars(strip_tags($_POST["topic"]))) ;
    $message_body = trim(htmlspecialchars(strip_tags($_POST["body"]))) ;
    
		$productID = intval($_POST['productID']);
    $slug = (!empty($_POST['slug'])) ? intval($_POST['slug']) : $productID;
		//echo $productID;
		$message_text = 'Вопрос от '.$customer_name.'('.$customer_email.')<br />';
		$message_text .= '<br />К товару <a href="http://teslocom.ru/product/'.$slug.'/">"'.$message_subject.'"<a/><br />';
		$message_text .= '<br />Текст вопроса:<br />'.$message_body;
		
		if(!valid_email($customer_email)){
			 $err_msg1 = 'Неверно введен E-mail';
			 $smarty->assign('err_msg1', $err_msg1);
			 RedirectSQ('productID='.$productID.'&ukey=product&err_msg');
		}
    
		//validate input data
		if (!empty($customer_email) && !empty($customer_name) && !empty($message_body) && !empty($message_subject) && !empty($message_text) && eregi("^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$", $customer_email)){
			
			if(CONF_ENABLE_CONFIRMATION_CODE){
			  
        if (!class_exists('KeyCAPTCHA_CLASS')) {
          include(DIR_CLASSES.'/class.keycaptcha.php');
        }
        $kc_o = new KeyCAPTCHA_CLASS();
        if (!$kc_o->check_result($_POST['capcode'])) {
          RedirectSQ('productID='.$productID.'&ukey=product');
        }
	     /*
				require_once(DIR_CLASSES.'/class.ivalidator.php');
				$iVal = new IValidator();
				if(!$iVal->checkCode($_POST['fConfirmationCode'])){
					//break;
					RedirectSQ('productID='.$productID.'&ukey=product');
				}*/
			}
			
			$customer_name = str_replace(array('@','<',"\n"), array('[at]', '', ''), $customer_name);
			$customer_email = str_replace(array("\n",'<'), '', $customer_email);
			//send a message to store administrator
			
			discAddDiscussion2( $productID, $customer_name, $message_subject, $message_body, $customer_email );
			//RedirectSQ('productID='.$productID.'&ukey=product');
			ss_mail(CONF_GENERAL_EMAIL,$message_subject,$message_text,true,
					array('From'=>'<'.$customer_email.'>','FromName'=>$customer_name));
	
			RedirectSQ('productID='.$productID.'&ukey=product');
		}else{
		  if(isset($_POST["add_topic"]))
		    RedirectSQ('productID='.$productID.'&ukey=product');
      else {
        if (empty($customer_name))
          $err_msg1 = 'Укажите Имя';
        if (empty($message_subject))
          $err_msg1 = 'Укажите Тему';        
        if (empty($customer_email))
          $err_msg1 = 'Укажите E-mail адрес';
        if (empty($message_body))
          $err_msg1 = 'Укажите Ваш вопрос';
        
        $smarty->assign('err_msg1', $err_msg1);
        RedirectSQ('productID='.$productID.'&ukey=product&err_msg');
      }
    }
?>