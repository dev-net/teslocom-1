$('.hndl_show_login').live('click', function(e) {
	getLayer('block-customerinfo').style.display = getElementComputedStyle('block-customerinfo', 'display')=='none'?"block":"none";
	getLayer('block-auth').style.display = getLayer('block-customerinfo').style.display=='none'?"block":"none";
	return false;
});

$('#hndl-show-billing-address').live('click', function(e) {
	getLayer('block-billing-address').style.display = this.checked?"none":"block";
});

$('#hndl-show-loginpass-fields').live('click', function() {
	getLayer('block-loginpass-fields').style.display = this.checked?"block":"none";
});

$('.country_box').live('change', function(e) {
	var objForm = getFormByElem(this);
	objForm['action'].value = 'update_form';
	objForm.submit();
	return false;
});

$('.autofill').live('focus', function(e) {
	if(this.value)return;

	var obj = getLayer(this.getAttribute('rel'));
	if(!obj)return;

	this.value = obj.value;
});