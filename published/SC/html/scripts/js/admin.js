function closeComment(commentId){

  var objBlock = getLayer(commentId);
  objBlock.style.display = "none";
  setCookie( commentId, 1, 1000, '/');
}

function checkGroupBoxState(group_box){

  group_box.checked = false;
  var boxes = getElementsByClass(group_box.getAttribute('rel'), document, 'input');
  for(var i_max = boxes.length-1; i_max>=0; i_max--){

    if(boxes[i_max].checked) continue;
    group_box.checked = false;
    return;
  }
  group_box.checked = true;
}

function getCountCheckGroupBox(rel){
  var count = 0;
  var boxes = getElementsByClass(rel, document, 'input');
  for(var i_max = boxes.length-1; i_max>=0; i_max--){
    if(!boxes[i_max].checked) continue;
    count++;
  }
  return count;
}

function sc_submitAjaxForm(objForm){

    var req = new JsHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
          if(req.responseText)alert(req.responseText);

      if(is_null(req.responseJS))return;

          if(req.responseJS._AJAXMESSAGE){

            var msgEntry = new Message();
            msgEntry.init(req.responseJS._AJAXMESSAGE);
            msgEntry.showMessage();
          }
        }
    }
  try {
    req.open('POST', set_query("&caller=1&initscript=ajaxservice"), true);
    req.send( { q: objForm } );
  } catch ( e ) { ; } finally {	;}
  return false;
}

$('input.input_message').live('focus', function() {
  this.className = this.className.replace(/input_message/ ,'')+' input_message_focus';
  if(this.value != this.getAttribute('rel'))return;

  this.value='';
}).live('blur', function() {
  if(this.value!='')return;
  this.className = this.className.replace(/input_message_focus/ ,'')+' input_message';
  this.value=this.getAttribute('rel');
});

$('.confirm_action').live('click', function(e) {
  //e.preventDefault();

  return window.confirm(this.getAttribute('title'));
});

$('.new_window').live('click', function(e) {
  e.preventDefault();

  var wnd_width = this.getAttribute('wnd_width');
  var wnd_height = this.getAttribute('wnd_height');

  open_window(this.href?this.href:this.getAttribute('rel'), wnd_width, wnd_height);
  return false;
});

$('.ajaxform').live('submit', function(e) {
  e.preventDefault();

  sc_submitAjaxForm(this);
  return false;
});

$('tr.gridline').live('mouseover', function() {
  this.style.background = '#f5f0bb';
}).live('mouseout', function() {
  this.style.background = '#fafae7';
});

$('tr.gridline1').live('mouseover', function() {
  this.style.background = '#f5f0bb';
}).live('mouseout', function() {
  this.style.background = '#ffffff';
});

$('input.goto').live('click', function(e) {
  e.preventDefault();

  var onPreClick =this.getAttribute('onpreclick');if(onPreClick != null);eval(onPreClick);
  if(this.className.search(/confirm/) !== -1 && !window.confirm(this.getAttribute('title')))return
  document.location.href = this.getAttribute('rel');
});

$('input.groupcheckbox').live('click', function(e) {
  var boxes = getElementsByClass(this.getAttribute('rel'), document, 'input');
  for(var i_max = boxes.length-1; i_max>=0; i_max--){

    boxes[i_max].checked = this.checked;
  }
});

$('input.checkbox').live('click', function(e) {
  checkGroupBoxState(getLayer(this.getAttribute('rel')));
  return;
});

$('.cancel_contentchanged').live('click', function(e) {
  //e.preventDefault();

  beforeUnloadHandler_contentChanged = false;
  return true;
});

$('.expand_languages').live('click', function(e) {
  e.preventDefault();

  getLayer(this.getAttribute('rel')).style.display = 'block';
  this.style.visibility = 'hidden';
});

$('.fade_div').live('click', function(e) {
  e.preventDefault();

  if(!sswgt_CartManager)return;
  sswgt_CartManager.shop_url = (window.WAROOT_URL != null) ? window.WAROOT_URL : conf_full_shop_url;
  sswgt_CartManager.showLayer(this.getAttribute('rel'), this.getAttribute('wnd_width'), this.getAttribute('wnd_height'));
});

var _countProducts = 0;
var _countSubProducts = 0;

/**
 * Show message
 * @param msg
 * @private
 */
var _showMessage = function(msg) {
  $('#progress_regenerate').show();
  $('#progress_regenerate span').html(msg);
}

/**
 * Ajax
 * @param params
 * @param callback
 * @param ukey
 * @private
 */
var _ajax = function(params, callback, ukey) {
  $.ajax({
    dataType: "json",
    url: "/index.php?ukey="+ukey,
    data: params
  }).done(function() {

  }).always(function( response ) {
      if (response.do == 'success') {
        callback(response);
      } else {
        _showMessage(response.msg);
      }
  })
}

/**
 * Create models
 * @param step
 * @param bid
 * @param re_compatibility
 * @param re_models
 * @private
 */
var _reModels = function(step, bid, re_compatibility, re_models) {
  var per_step = 25;
  _showMessage('Обработано '+step+' из '+_countProducts+' продуктов. Продолжаю обработку... <img src="/i/loading.gif" style="height: 15px;" />');
  var params = {action: 'regeneratePerStep', step: step, per_step: per_step, bid: bid, re_compatibility: re_compatibility, re_models: re_models};
  if (step <= _countProducts) {
    _ajax(params, function(response) {
      step += per_step;
      _reModels(step, bid, re_compatibility, re_models);
    }, 'categorygoods');
  } else {
    _showMessage('Обработка завершена.');
  }
}

/**
 * Clear models
 * @param bid
 * @param callback
 * @private
 */
var _clearModels = function(bid, callback){
  var params = {action: 'clearModels', bid: bid};
  _ajax(params, function(response) {
    _showMessage("Модели очищены. Продолжаем...");
    callback(response);
  }, 'categorygoods');
}

/**
 * Create brands
 * @param re_compatibility
 * @param re_models
 * @private
 */
var _reBrands = function(re_compatibility, re_models) {
  var params = {action: 'clearBrands'};
  _ajax(params, function(response) {
    _showMessage("Бренды пересозданы. Продолжаем...");
    _clearModels(0, function(response) {
      _reModels(0, 0, re_compatibility, re_models);
    });
  }, 'categorygoods');
}

/**
 * Create series.
 * @param series
 * @param callback
 * @private
 */
var _reSeries = function(series, callback) {
  var params = {action: 'reSeries', series: series};
  _ajax(params, function(response) {
    _showMessage("Серии пересозданы. Продолжаем...");
    callback(response);
  }, 'categorygoods');
}

/**
 * Get count of products
 * @param bid
 * @param re_compatibility
 * @param callback
 * @private
 */
var _getCount = function(bid, re_compatibility, callback) {
  var params = {action: 'countProducts', bid: bid, re_compatibility: re_compatibility};
  _ajax(params, callback, 'categorygoods');
}

/**
 * Do regenarate models
 * @param re_brands
 * @param re_models
 * @param re_compatibility
 * @param bid
 * @param response
 * @private
 */
var _doRegenerate = function(re_brands, re_models, re_compatibility, bid, response) {
  if (re_brands) {
    _reBrands(re_compatibility, re_models);
  }
  else {
    if (re_models != 3) {
      _clearModels(bid, function(response) {
        _reModels(0, bid, re_compatibility, re_models);
      });
    } else {
      if (re_compatibility) {
        _reModels(0, bid, re_compatibility, re_models);
      } else {
        _showMessage('Обработка завершена.');
      }
    }
  }
}

$('.regen_brands').live('click', function(e) {
  e.preventDefault();

  var re_brands = 0, bid = 0, re_compatibility = 0, re_models = 0, re_series = 0;
  re_models = $("[name='select_models']:checked").val();
  if ($("[name='select_models']:checked").val() == 2) {
    if ($("[name='select_brand'] option:selected").val() > 0) {
      bid = $("[name='select_brand'] option:selected").val();
    }
  }

  if ($("[name='re_compatibility']").is(':checked')) {
    re_compatibility = 1;
  }

  if ($("[name='re_brands']").is(':checked')) {
    re_brands = 1;	re_models = 0;	bid = 0;
  }

  if ($("[name='re_series']").is(':checked')) {
    re_series = 1;
  }

  _showMessage("Начинаю обработку...");
  _getCount(bid, re_compatibility, function(response) {
    _showMessage('Обнаружено '+response.all+' продуктов, из них '+response.count+' будут обработаны. Начинаю обработку...');
    _countProducts = response.count;
    if (response.count > 0) {
      if (re_series) {
        var series = $('[name="series"]').val();
        _reSeries(series, function(){
          _doRegenerate(re_brands, re_models, re_compatibility, bid, response);
        });
      } else {
        _doRegenerate(re_brands, re_models, re_compatibility, bid, response);
      };
    };
  });
});

var additional = additional || {};

additional.counProducts = 0;
additional.counSubProducts = 0;
additional.ids = [];

/**
 *
 * @param callback
 */
additional.getCountProducts = function(callback){
  _showMessage('Получение кол-ва товаров...  <img src="/i/loading.gif" style="height: 15px;" />');

  var params = {action: 'AJAXcountProducts', ids: additional.ids};
  _ajax(params, callback, 'additional_products');
};

/**
 *
 * @param callback
 */
additional.clearItems = function(callback){
  _showMessage('Начинаю очистку итемов...  <img src="/i/loading.gif" style="height: 15px;" />');

  var params = {action: 'AJAXclearItems'};
  _ajax(params, function(response) {
    _showMessage("Итемы очищены. Продолжаем...");

    callback(response);
  }, 'additional_products');
}

/**
 *
 * @param step
 */
additional.reItems = function(step){
  _showMessage("Начинаю создание итемов...");

  var per_step = 100;
  var params = {action: 'AJAXreItemsPerStep', step: step, per_step: per_step, ids: additional.ids};

  _showMessage('Обработано '+step+' из '+additional.counProducts+' продуктов. Продолжаю обработку... (Создание итемов) <img src="/i/loading.gif" style="height: 15px;" />');

  if (step <= additional.counProducts) {
    _ajax(params, function(response) {
      step += per_step;
      additional.reItems(step);
    }, 'additional_products');
  } else {
    _showMessage("Начинаю создание подтоваров...");

    additional.clearSubs(function(){
      additional.reSubs(0);
    });
  }
};

/**
 *
 * @param callback
 */
additional.clearSubs = function(callback){
  _showMessage('Начинаю очистку подтоваров...  <img src="/i/loading.gif" style="height: 15px;" />');

  var params = {action: 'AJAXclearSubs'};
  _ajax(params, function(response) {
    _showMessage("Подтовары очищены. Продолжаем...");

    callback(response);
  }, 'additional_products');
}

/**
 *
 * @param step
 */
additional.reSubs = function(step){
  _showMessage("Начинаю создание подтоваров...");

  var per_step = 100;
  var params = {action: 'AJAXreSubsPerStep', step: step, per_step: per_step, ids: additional.ids};

  _showMessage('Обработано '+step+' из '+additional.counProducts+' продуктов. Продолжаю обработку... (Создание подтоваров) <img src="/i/loading.gif" style="height: 15px;" />');
  if (step <= additional.counProducts) {
    _ajax(params, function(response) {
      step += per_step;
      additional.reSubs(step);
    }, 'additional_products');
  } else {

    if ($('#reNoIndex').is(':checked')) {
      _showMessage("Начинаю обработку индексации подтоваров...");

      additional.getCountSubProducts(function(response) {
        additional.counSubProducts = response.count;
        if (additional.counSubProducts > 0) {
          _showMessage(additional.counSubProducts+' подтоваров будут обработаны. Начинаю обработку...');

          additional.clearNoIndex(function(){
            additional.reNoIndex(0);
          });
        };
      });
    } else {
      additional.finished();
    }
  }
};

/**
 *
 * @param callback
 */
additional.getCountSubProducts = function(callback){
  _showMessage('Получение кол-ва дубликатов... <img src="/i/loading.gif" style="height: 15px;" />');

  var params = {action: 'AJAXcountSubProducts'};
  _ajax(params, callback, 'additional_products');
};

/**
 *
 * @param callback
 */
additional.clearNoIndex = function(callback){
  _showMessage('Начинаю очистку noindex у дубликатов...  <img src="/i/loading.gif" style="height: 15px;" />');

  var params = {action: 'AJAXclearNoIndex'};
  _ajax(params, function(response) {
    _showMessage("Подтовары очищены. Продолжаем...");

    callback(response);
  }, 'additional_products');
}

/**
 *
 * @param step
 */
additional.reNoIndex = function(step){
  _showMessage("Начинаю обработку ноидекс параметра...");

  var per_step = 50;
  var params = {action: 'AJAXreNoIndexPerStep', step: step, per_step: per_step};

  _showMessage('Обработано '+step+' из '+additional.counSubProducts+' дубликатов. Продолжаю обработку... <img src="/i/loading.gif" style="height: 15px;" />');
  if (step <= additional.counSubProducts) {
    _ajax(params, function(response) {
      step += per_step;
      additional.reNoIndex(step);
    }, 'additional_products');
  } else {
    additional.finished();
  }
};

/**
 *
 */
additional.finished = function() {
  _showMessage('Пересчет кол-ва...  <img src="/i/loading.gif" style="height: 15px;" />');

  var params = {action: 'AJAXsetCountSubProducts'};
  _ajax(params, function(){
    _showMessage('Обработка завершена.');
  }, 'additional_products');
}

$('.regen_subproducts').live('click', function(e) {
  e.preventDefault();


  var ids = [];
  $("#categories .jstree-clicked").each(function () {
    ids.push($(this).parent('li').data('id'));
  });
  additional.ids = ids.join();

  _showMessage("Начинаю обработку...");

  additional.getCountProducts(function(response) {
    additional.counProducts = response.count;
    if (additional.counProducts > 0) {
      _showMessage(additional.counProducts+' продуктов будут обработаны. Начинаю обработку...');

      if ($('#reItems').is(':checked')) {
        additional.clearItems(function(){
          additional.reItems(0);
        });
      } else {
        if ($('#reSubs').is(':checked')) {
          additional.clearSubs(function(){
            additional.reSubs(0);
          });
        } else {
          _showMessage("Начинаю обработку индексации подтоваров...");

          additional.getCountSubProducts(function(response) {
            additional.counSubProducts = response.count;
            if (additional.counSubProducts > 0) {
              _showMessage(additional.counSubProducts+' дубликатов будут обработаны. Начинаю обработку...');

              additional.clearNoIndex(function(){
                additional.reNoIndex(0);
              });
            };
          });
        }
      }
    };
  });
});

/**
 * Toggle show content
 */
if ($('[data-show]')) {
  $('[data-show]').live('click', function(e){
    e.preventDefault();

    if ($(this).hasClass('btn-show')){
      $(this).hide();
      $($(this).data('content')).show();
    }
    if ($(this).hasClass('btn-hide')){
      $(this).parents('[data-wrapper]').eq(0).find('.btn-show').eq(0).show();
      $($(this).data('content')).hide();
    }
  });
};

if ($('[data-checkbox-show]')) {
  $('[data-checkbox-show]').live('click', function(e){
      $($(this).data('content')).show();
  });
};

$(document).ready(function() {

  $("a.iframe").fancybox({
    'autoScale': true,
    'transitionIn': 'fade',
    'transitionOut': 'fade',
    'type': 'iframe'
  });

});