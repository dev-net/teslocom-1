function ProductIsProgramHandler(){
  
  document.getElementById('FileNameTable').style.display = document.MainForm.ProductIsProgram.checked?'block':'none';
  document.MainForm.eproduct_filename.disabled = !document.MainForm.ProductIsProgram.checked;
  document.MainForm.eproduct_available_days.disabled = !document.MainForm.ProductIsProgram.checked;
  document.MainForm.eproduct_download_times.disabled = !document.MainForm.ProductIsProgram.checked;
}

function picts_removeImage(photoID){

  getLayer("product-pictures-container").deleteRow(getLayer("picture-container-"+photoID).rowIndex);
}

function picts_setDefault(photoID){
  
  var old_default_container = getElementsByClass('default_picture', getLayer("product-pictures-container"),'td');
  if(old_default_container.length){
    
    old_default_container[0].className = old_default_container[0].className.replace(/default_picture/, '');
  }
  
  getLayer('picture-container-'+photoID).className += " default_picture";
}

function picts_addImage(picture){

var tableTag = getLayer("product-pictures-container");
var cntTag = createTag('tbody',tableTag);
//var trTag = document.createElement('tr');
var trTag = createTag('tr',cntTag);

//var trTag = cntTag.insertRow(cntTag.rows.length);

  cntTag.className = "dragable";
  trTag.className = "gridline1";
  
  trTag.setAttribute("height",tableTag.getAttribute("trheight"));
  
  trTag.id = "picture-container-"+picture.photoID;
  
  var tdTag = trTag.insertCell(0);
  tdTag.className = "img_container handle";

  var imgTag = createTag('img', tdTag);
  imgTag.src = picture['thumbnail_url'];
  
  var tdTag = trTag.insertCell(1);
  tdTag.className = "handle";
  if(is_null(picture.large_picture.url)){
    tdTag.innerHTML = picture.large_picture.file+'<br>'+translate.str_image_not_uploaded;
  }else{
    tdTag.innerHTML=picture.thumbnail_picture.file+'<br>'+picture.thumbnail_picture.width+'&times;'+picture.thumbnail_picture.height+'&nbsp;px<br>'+picture.thumbnail_picture.size;
  
  }
  var tdTag = trTag.insertCell(2);
  if(is_null(picture.large_picture.url)){
    tdTag.innerHTML = picture.large_picture.file+'<br>'+translate.str_image_not_uploaded;
  }else{
    var aTag = createTag('a', tdTag);
    aTag.href = picture.large_picture.url;
    aTag.className = "new_window bluehref";
    aTag.setAttribute("wnd_width", picture.large_picture.width);
    aTag.setAttribute("wnd_height", picture.large_picture.height);
    aTag.innerHTML=picture.large_picture.file;
    tdTag.innerHTML=tdTag.innerHTML+' '+picture.large_picture.width+'&times;'+picture.large_picture.height+'&nbsp;px<br>'+picture.large_picture.size;
  
  }
  var tdTag = trTag.insertCell(3);
  if(is_null(picture.enlarged_picture.url)){
    tdTag.innerHTML = picture.enlarged_picture.file+'<br>'+translate.str_image_not_uploaded;
  }else{
    var aTag = createTag('a', tdTag);
    aTag.href = picture.enlarged_picture.url;
    aTag.className = "new_window bluehref";
    aTag.setAttribute("wnd_width", picture.enlarged_picture.width);
    aTag.setAttribute("wnd_height", picture.enlarged_picture.height);
    aTag.innerHTML=picture.enlarged_picture.file;
    tdTag.innerHTML=tdTag.innerHTML+' '+picture.enlarged_picture.width+'&times;'+picture.enlarged_picture.height+'&nbsp;px<br>'+picture.enlarged_picture.size;
    
  }
  
  var tdTag = trTag.insertCell(4);
  //thumbnail td
  //thumbnail info td
  //enlarged picture info
  //picture info
  
  var aTag = createTag('a', tdTag);
  aTag.href = "#delete_picture";
  aTag.className = "delete_picture_handlers";
  aTag.setAttribute("photoID", picture.photoID);
  var imgTag = createTag('img', aTag);
  imgTag.src = './images/remove.gif';
  imgTag.alt = translate.prdset_btn_delete_pict;
  
  var iTag = createTag('span',tdTag);
  iTag.innerHTML = '<input type="hidden" class="field_priority" name="priority_'
          +picture.photoID
          +'" value="'
          +(tableTag.rows.length-2)
          +'"/>';

  //Behaviour.apply();
  dragsort.makeListSortable(tableTag);
  
}

$('a.delete_picture_handlers').live('click', function(e) {
	e.preventDefault();

	if(!window.confirm(translate.prdset_msg_confirm_pict_delete))return false;
	var req = new JsHttpRequest();
	var photoID = this.getAttribute('photoID');

	picts_removeImage(photoID);

	req.onreadystatechange = function(){

		if (req.readyState != 4)return;
		var pictures_container = getLayer("product-pictures-container");
		getLayer('set-default').value = pictures_container.rows.length>1?0:1;
		if(req.responseText)alert(req.responseText);

		if(!is_null(req.responseJS) && req.responseJS._AJAXMESSAGE){
			var msgEntry = new Message();
			msgEntry.init(req.responseJS._AJAXMESSAGE);
			if(!msgEntry.isSuccess()){
				alert(msgEntry.getMessage());
				return;
			}
		}

	};

	try {
		req.open('GET', document.location.href.replace(/\#.*$/, '')+"&caller=1&initscript=ajaxservice", true);
		req.send({'action': 'delete_picture','photoID':photoID});
	} catch ( e ) {
		catchResult(e);
	} finally { ;}
	return false;
});

$('a.set_default_picture_handlers').live('click', function(e) {
	e.preventDefault();

	var req = new JsHttpRequest();
	var photoID = this.getAttribute('photoID');

	req.onreadystatechange = function(){

		if (req.readyState != 4)return;

		if(req.responseText)alert(req.responseText);
		if(!is_null(req.responseJS) && req.responseJS._AJAXMESSAGE){

			var msgEntry = new Message();
			msgEntry.init(req.responseJS._AJAXMESSAGE);

			if(!msgEntry.isSuccess()){
				alert(msgEntry.getMessage());
				return;
			}
		}

		picts_setDefault(photoID);
	};

	try {
		req.open(null, document.location.href.replace(/\#.*$/, '')+"&caller=1&initscript=ajaxservice", true);
		req.send({'action': 'set_default_picture','photoID':photoID, 'productID':getLayer('product-id').value});
	} catch ( e ) {
		catchResult(e);
	} finally { ;}

	return false;
});

$('#upload-url').live('focus', function(e) {
	if(this.value!='URL')return;
	this.value = '';
}).live('blur', function() {
	if(this.value&&this.value!='http://')return;
	this.value = 'URL';
});

$('#image-source-file').live('click', function() {
	getLayer('upload-browse').disabled = !this.checked;
	getLayer('upload-url').disabled = this.checked;
});

$('#image-source-url').live('click', function() {
	getLayer('upload-browse').disabled = this.checked;
	getLayer('upload-url').disabled = !this.checked;
});

$('#product-settings-form input').live('change', function() {
	if(this.id != 'upload-browse')beforeUnloadHandler_contentChanged = true;
});

$('#product-settings-form textarea').live('change', function() {
	beforeUnloadHandler_contentChanged = true;
});

$('#product-settings-form select').live('change', function() {
	beforeUnloadHandler_contentChanged = true;
});


getLayer('btn-save-action').onclick = function(){beforeUnloadHandler_contentChanged = false;};
getLayer('do-upload-handler').onclick = function(){
      
  if(!getLayer("upload-browse").value&&((getLayer("upload-url").value=='URL')||(getLayer("upload-url").value=='http://')))return;
  //try {
  getLayer('field-skip-image-upload').value = "1";
  getLayer('do-upload-handler').style.display = "none";
  getLayer('processing-image').style.display = "";
  //SEE:
  var btn_save = getLayer('btn-save-action');
  if(btn_save){
    btn_save.disabled = true;
    btn_save.style.color = "#ccc";  
  }
    
    
  var req = new JsHttpRequest();
  req.onreadystatechange = function() {

   if (req.readyState == 4) {

    getLayer('action-name').value = 'save_product';
    getLayer('do-upload-handler').style.display = "";
    getLayer('processing-image').style.display = "none";
    //SEE:
    getLayer('btn-save-action').disabled = false;
    getLayer('btn-save-action').style.color = "black";
    getLayer('field-skip-image-upload').value = "0";
        
    if(req.responseText)alert(req.responseText);
    if(is_null(req.responseJS))return;

      if(req.responseJS._AJAXMESSAGE){
        
        var msgEntry = new Message();
        msgEntry.init(req.responseJS._AJAXMESSAGE);
        
        if(!msgEntry.isSuccess()){
        alert(msgEntry.getMessage());
          return;
        }        
      }
      
      if(!req.responseJS.picture)return;
      var pictures_container = getLayer("product-pictures-container");
      getLayer('set-default').value = pictures_container.rows.length>1?0:1;

      picts_addImage(req.responseJS.picture);
      
      var who = getLayer("upload-browse");  
      who2 = document.createElement('input');
      var att= new Array('type', 'size', 'id', 'name');
      for(i = 0;i< att.length;i++){
        who2.setAttribute(att[i],who.getAttribute(att[i]));
      }

      who.parentNode.replaceChild(who2,who);
        
      getLayer("product-id").value = req.responseJS.productID;
      getLayer("make-slug-id").value = req.responseJS.productID;
      getLayer("upload-url").value = 'URL';

      }
    };
    //SEE:
  getLayer('action-name').value = 'upload_picture';
  getLayer('upload-priority').value = getLayer("product-pictures-container").rows.length-1;
  var old_default_container = getElementsByClass('default_picture', getLayer("product-pictures-container"),'tr');

  try {
    req.open('POST', document.location.href.replace(/\#.*$/, '')+"&caller=1&initscript=ajaxservice", true);
    req.send( { q: getLayer("product-settings-form") } );
  } catch ( e ) {
    catchResult(e);
    //getLayer('btn-save-action').disabled = false;
    getLayer('btn-save-action').style.color = "black";
    getLayer('field-skip-image-upload').value = "0";
  } finally { ;}
  
  return false;
};
