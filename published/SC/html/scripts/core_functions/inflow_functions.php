<?php
/**
 * Inflow functions
 */

/**
 * Get inflow
 * @param $iid
 * @return array|bool
 */
function NFLWget($iid) {
  $q = db_query("SELECT * FROM `SC_product_inflows` WHERE id='{$iid}'");
  $inflow = db_fetch_row($q);
  if ($inflow)
    return $inflow;
  return false;
}

/**
 * @param int $page
 * @param int $show_all
 * @return array
 */
function NFLWgets($page = 0, $show_all = 0) {

  if ($show_all) {
    $limit = "0, 10";
  } else {
    $limit = "{$page} , 1";
  }

  $is = array();
  $q = db_query("SELECT * FROM `SC_product_inflows` ORDER BY date DESC LIMIT {$limit}");
  while ($r = db_fetch_row($q)){

    $r['date'] = NFLWgetDate($r['date']);
    $r['items'] = NFLWgetItems($r['id']);
    if (!empty($r['items']))
      $is[] = $r;
  }

  return $is;
}

/**
 * @param $date
 * @return mixed
 */
function NFLWgetDate($date){
  return str_replace(array('January','February','March','April','May','June','July','August','September','October','November','December'),
    array('января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'), date('d F, Y', $date));
}

/**
 * Get items for inflow
 * @param $iid
 * @return array
 */
function NFLWgetItems($iid) {
  $its = array();
  $q = db_query("SELECT * FROM `SC_product_inflow_items` WHERE iid=".$iid);

  $prtnr = 0;
  if (isset($_SESSION['is_partner'])){
    if (isset($_SESSION['choose_p'])){
      $prtnr = PRTNRgetFromId(intval($_SESSION['choose_p']));
    }
    else {
      $prtnr = PRTNRget();
    }
    $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID']);
  }

  while ($r = db_fetch_row($q)){
    $r['product'] =  GetProduct($r['pid'], 0, $prtnr);

    $days_ago = mktime(0, 0, 0, date("m"),   date("d")-90,   date("Y"));
    if ($days_ago <= strtotime($r['product']['date_added'])){
      $r['product']['is_new'] = 1;
    }

    $pq = db_query("SELECT * FROM `SC_product_pictures` WHERE productID={$r['pid']} ORDER BY priority LIMIT 0,1");
    $picture_row = db_fetch_row($pq);
    if( isset($picture_row["photoID"]) ) $r['product']["default_picture"] = $picture_row["photoID"];
    else $picture_row = null;

    if ( $picture_row ){
      $r['product']["picture"] = $picture_row['filename'];
      $r['product']["thumbnail"] = $picture_row['thumbnail'];
      $r['product']["big_picture"] = $picture_row['enlarged'];
      $r['product']['photoID'] = $picture_row['photoID'];
    }

    if (!file_exists(DIR_PRODUCTS_PICTURES."/".$r['product']["picture"] )) $r['product']["picture"] = '';
    if (!file_exists(DIR_PRODUCTS_PICTURES."/".$r['product']["thumbnail"] )) $r['product']["thumbnail"] = '';
    if (!file_exists(DIR_PRODUCTS_PICTURES."/".$r['product']["big_picture"] )) $r['product']["big_picture"] = '';
    else if ($r['product']["big_picture"]){
      $size = getimagesize(DIR_PRODUCTS_PICTURES."/".$r['product']["big_picture"] );
      $r['product']['picture_width'] = $size[0]+40;
      $r['product']['picture_height'] = $size[1]+30;
    }

    if(!$r['product']['picture'] && !$r['product']['thumbnail']){
      $r['product']['picture'] = '';
      $r['product']['thumbnail'] = '';
      $r['product']['big_picture'] = '';
    }

    $its[] = $r;
  }

  return $its;
}

/**
 * Get date of last visit
 * @param $uid
 * @return bool
 */
function NFLWgetLastVisit($uid){
  $q = db_query("SELECT * FROM `SC_product_inflow_lastvisit` WHERE uid='{$uid}'");
  $lv = db_fetch_row($q);
  if (!empty($lv)){
    return $lv['date'];
  }
  return false;
}

/**
 * Set last visit
 * @param $uid
 * @return int
 */
function NFLWsetLastVisit($uid){
  $date = time();
  if (!NFLWgetLastVisit($uid)){
    $sql = "INSERT INTO `SC_product_inflow_lastvisit` (uid, date) VALUES ({$uid}, {$date})";
    db_query($sql);
  } else {
    $sql = "UPDATE `SC_product_inflow_lastvisit` SET date = '{$date}'  WHERE uid = {$uid}";
    db_query($sql);
  }
  return $date;
}

/**
 * Get quantity of new inflow
 * @param $last_visit
 * @return int
 */
function NFWLgetQuantityNewInflow($last_visit){
  $quantity = 0;
  $q = db_query("SELECT * FROM `SC_product_inflows` WHERE date >= '{$last_visit}'");
  while ($r = db_fetch_row($q)){
    $items = NFLWgetItems($r['id']);
    foreach ($items as $item)
      $quantity += intval($item['quantity']);
  }
  return $quantity;
}

/**
 * Add new inflow
 * @return int
 */
function NFLWadd(){
  $date = time();
  $sql = "INSERT INTO `SC_product_inflows` (date) VALUES ({$date})";
  db_query($sql);
  return db_insert_id();
}

/**
 * @param $code
 * @param $quantity
 * @param $iid
 * @return bool
 */
function NFLWaddItem($code, $quantity, $iid){
  $code = trim($code);
  $quantity = intval($quantity);

  if ($product = getProductFromCode($code)) {
    $sql = "INSERT INTO `SC_product_inflow_items` (iid, pid, quantity) VALUES ('{$iid}', '{$product['productID']}', '{$quantity}')";
    db_query($sql);
    return true;
  }
  return false;
}

/**
 * @return mixed
 */
function NFLWgetCount() {
  $q = db_query("SELECT count(*) as c FROM `SC_product_inflows`");
  $r = db_fetch_row($q);
  return $r['c'];
}

/**
 * @param int $page
 * @param int $show_all
 * @return mixed
 */
function NFLWgetNavigator($page = 0, $show_all = 0) {
  $count = NFLWgetCount();
  if ($show_all) {
    $page = 'show_all';
  }

  ShowNavigator($count, $page, 1, '', $out);
  return $out;
}

?>