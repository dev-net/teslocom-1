<?php

/**
 * Check if partner
 * @return bool
 */
function PRTNRis(){
  $login = $_SESSION["log"];
  if (!empty($login)){
    $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE Login='{$login}' && is_partner=1");
    $p = db_fetch_row($q); 
    if (!empty($p))
      return true;
  }
  return false;
}

/**
 * Get partner current
 * @return bool
 */
function PRTNRisManager(){
  $login = $_SESSION["log"];
  if (!empty($login)){
    $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE Login='{$login}' && is_partner=1 && is_manager = 1");
    $p = db_fetch_row($q); 
    if (!empty($p))
      return true;
  }
  return false;
}

/**
 * Get partner current
 * @return array|bool
 */
function PRTNRget(){
  $login = $_SESSION["log"];
  if (!empty($login)){
    $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE Login='{$login}' && is_partner=1");
    $p = db_fetch_row($q);   
    
    if (!empty($p)){
      $r['reg_fields'] = PRTNRgetRegFields($p['customerID']);  
      return $p;    
    }
  } 
  return false; 
}

/**
 * Get partners
 * @return array
 */
function PRTNRgets(){
  $ps = array();
  $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE is_partner=1");
  while ($r = db_fetch_row($q)){
    
    $r['reg_fields'] = PRTNRgetRegFields($r['customerID']);
    
    $ps[] = $r;
  }    
  return $ps;
}

/**
 * Get partners from cat
 * @param $cid
 * @return array
 */
function PRTNRgetsFromCat($cid) {
  $ps = array();  
  $q = db_query("SELECT c.*, cu.* FROM ".CUSTOMERS_TABLE." as c 
                 LEFT JOIN `SC_categories_user` as cu ON cu.cid = {$cid} and cu.uid = c.customerID                
                 WHERE c.is_partner=1");
  while ($r = db_fetch_row($q)){
    $nw_price = PRTNRgetNWPrice($r, $cid);
    if (!empty($nw_price))
      $r['nw_price'] = $nw_price;

    $r['reg_fields'] = PRTNRgetRegFields($r['customerID']);
    
    $ps[] = $r;
  }
  
  return $ps;
}

/**
 * Get additional fields
 * @param $uid
 * @return array
 */
function PRTNRgetRegFields($uid){
  $rfvs = array();     
  $reg_q = db_query("SELECT rf.reg_field_name_ru as name, rfv.reg_field_value as value FROM `SC_customer_reg_fields_values` as rfv 
                     LEFT JOIN `SC_customer_reg_fields` as rf ON rf.reg_field_ID = rfv.reg_field_ID
                     WHERE rfv.customerID=".$uid);
  while ($reg_r = db_fetch_row($reg_q)){
    $rfvs[] = array('name' => $reg_r['name'], 'value' => $reg_r['value']);
  }
  
  return $rfvs;
}

/**
 * Get field
 * @param $rfvs
 * @param $field
 * @return mixed
 */
function PRTNRgetRegField ($rfvs, $field) {
  foreach ($rfvs as $rfv){
    if (strpos($rfv['name'], $field) !== FALSE)
      return $rfv['value'];
  }  
}

/**
 * Get managers
 * @return array
 */
function PRTNRgetManagers(){
  $ps = array();
  $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE is_partner=1 && is_manager=1");
  while ($r = db_fetch_row($q)){
    $ps[] = $r;
  }  
  return $ps;
}

/**
 * Get manager default
 * @return array
 */
function PRTNRgetManagerDefault(){

  $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE is_partner=1 && is_manager=1 && m_default=1");
  $p = db_fetch_row($q);
  if (!empty($p))
    $p['Phone'] = PRTNRgetRegField(PRTNRgetRegFields($p['customerID']), 'Телефон');
    return $p;
}

/**
 * Get partner from id
 * @param $uid
 * @param int $with_additional
 * @return array|bool
 */
function PRTNRgetFromId($uid, $with_additional = 0){
  $uid = intval($uid);
  if (!empty($uid)){
    $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE customerID={$uid} && is_partner=1");
    $p = db_fetch_row($q);
    if (!empty($p))
      if ($with_additional)
        $p['reg_fields'] = PRTNRgetRegFields($p['customerID']);
      return $p;    
  } 
  return false; 
}

/**
 * Get wprice of customer.
 * @param $p
 * @param int $cid
 * @return int
 */
function PRTNRgetNWPrice($p, $cid = 0){
  $nw_price = 0;

  if (!empty($p['customerID'])) {
    if (isset($p['w_price']) && !empty($p['w_price'])){
      $nwp = intval($p['w_price']);
      if ($nwp > 0 && $nwp < 5){
        $nw_price = $nwp;
      }
    }
     
    if (isset($cid) && !empty($cid)){
      $nwp = PRTNRgetCatNWPrice($cid, $p['customerID']);
      if (!empty($nwp))
        $nw_price = $nwp;
    }
  }  
  return $nw_price;
}

/**
 * Get list of categories.
 * @param $uid
 * @return array
 */
function PRTNRgetNWPs($uid)
{
  $nwps = array();
  $q = db_query("SELECT * FROM SC_categories_num_user WHERE uid = {$uid} ") or die (db_error());
  while ($nwp = db_fetch_assoc($q)) {
    $nwps[] = $nwp;
  }
  return $nwps;
}

/**
 * Get recursive num of w_prices
 * @param $cid
 * @param $uid
 * @return int
 */
function PRTNRgetCatNWPrice($cid, $uid){
  $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID={$cid}");
  $cat = db_fetch_row($q);
  $nw_price = 0;
  
  if ($cat['parent'] > 0 ){
    if ($nwp = PRTNrgetCatNWP($cat['categoryID'], $uid))
      $nw_price = intval($nwp['nw_price']);
    else {
       $nw_price = PRTNRgetCatNWPrice($cat['parent'], $uid);
    }
  } else {
    if ($nwp = PRTNrgetCatNWP($cat['categoryID'], $uid))
      $nw_price = intval($nwp['nw_price']);
  }  
  return $nw_price;
}

/**
 * Get NWP
 * @param $cid
 * @param $uid
 * @return array|bool
 */
function PRTNrgetCatNWP($cid, $uid){
  $q = db_query("SELECT * FROM `SC_categories_num_user` WHERE cid={$cid} AND uid={$uid}");
  $nwp = db_fetch_row($q);
  if (!empty($nwp))
    return $nwp;
   
  return false;  
}

/**
 * Get recursive num of w_prices
 * @param $cid
 * @param $uid
 * @param $nw_price
 * @return float|int
 */
function PRTNRgetCatWPrice($cid, $uid, $nw_price){
  $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID={$cid}");
  $cat = db_fetch_row($q);
  $w_price = 0;
  
  if ($cat['parent'] > 0 ){
    if ($wp = PRTNRgetCatWP($cat['categoryID'], $uid))
      $w_price = floatval($wp["w_price_{$nw_price}"]);
    else {
       $w_price = PRTNRgetCatWPrice($cat['parent'], $uid, $nw_price);
    }
  }else{
    if ($wp = PRTNRgetCatWP($cat['categoryID'], $uid))
      $w_price = floatval($wp["w_price_{$nw_price}"]);
  }
  return $w_price;
}

/**
 * Get WP
 * @param $cid
 * @param $uid
 * @return array|bool
 */
function PRTNRgetCatWP($cid, $uid){
  $q = db_query("SELECT * FROM `SC_categories_user` WHERE cid={$cid} AND uid={$uid}");
  $wp = db_fetch_row($q);
  if (!empty($wp))
    return $wp;
   
  return false;  
}

/**
 * Get wprice for product from category
 * @param $uid
 * @param $cid
 * @param $nw_price
 * @return float|int
 */
function PRTNRgetCategoryWPrice($uid, $cid, $nw_price){
  $w_price = 0;

  if (!empty($cid)) {
    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE categoryID={$cid}");
    $cat = db_fetch_row($q);

    if (!empty($p['customerID'])) {
      $w_price = PRTNRgetCatWPrice($cid, $uid, $nw_price);
    }

    if (empty($w_price)) {
      if (isset($cat["w_price_{$nw_price}"]) && !empty($cat["w_price_{$nw_price}"])){
        $w_price = floatval($cat["w_price_{$nw_price}"]);
      }
    }
  }  
  return $w_price;
}

/**
 * Delete nwp
 * @param $cid
 * @param $uid
 */
function PRTNRdelNWP($cid, $uid){
  $sql = "DELETE FROM `SC_categories_num_user` WHERE cid = {$cid} AND uid = {$uid}";
  db_query($sql);
}

/**
 * Add wp
 * @param $cid
 * @param $uid
 * @param $nwp
 */
function PRTNRaddNWP($cid, $uid, $nwp){
  $sql = "INSERT INTO `SC_categories_num_user` (nw_price, cid, uid) VALUES ({$nwp}, {$cid}, {$uid})";
  db_query($sql);
}

/**
 * Save partner info
 * @param $data
 */
function PRTNRsave($data){
  $fields = array('w_price', 'is_partner', 'is_manager', 'm_default', 'parent_partner');
  
  $strings = array();
  foreach ($fields as $field){
    if (!isset($data[$field]))
      $data[$field] = 0;
    
    $strings[] = $field.' = \''.mysql_real_escape_string(xStripSlashesGPC($data[$field])).'\'';    
  }
  
  $sql = "UPDATE ".CUSTOMERS_TABLE." SET ".implode(', ', $strings)." 
            WHERE customerID = {$data['customerID']}";
  db_query($sql);
}

/**
 * @param $p
 * @param int $prtnr
 * @param int $is_admin
 * @param int $nw_price
 * @return mixed
 */
function _PRTNRgeneratePrice($p, $prtnr = 0, $is_admin = 0, $nw_price = 0) {

  if (empty($is_admin)) {
    if (!empty($prtnr)) {
      if (empty($nw_price))
        $nw_price = PRTNRgetNWPrice($prtnr, $p['categoryID']);

      $original_price = floatval($p['Price']);
      $w_price_4 = floatval($p["w_price_4"]);
      $p['prices']['original'] = $original_price;

      if ($nw_price > 0){
        $p_discount =  floatval($p["w_price_{$nw_price}"]);

        if ($nw_price > 0 && $nw_price < 5){
          if ($nw_price == 4) {
            if ($p_discount > 0)
              $p['Price'] = $p_discount;
          } else {
            if (!empty($w_price_4)){
              if ($p_discount > 0)
                $p['Price'] = $p_discount + $w_price_4;
              else {
                $c_discount = PRTNRgetCategoryWPrice($p['customerID'], $p['categoryID'], $nw_price);
                if ($c_discount > 0)
                  $p['Price'] = $c_discount + $w_price_4;
              }
            }
          }
        }
        //$product["PriceWithUnit"]	= show_price($product['Price']);

        if ($original_price != $p['Price'])
          $p['PriceOriginal'] = "Розница: ".show_price($original_price);
      }

      if ($prtnr['is_manager']) {
        for ($i = 1; $i <= 3; $i++)
          $p['prices']["wp{$i}"] = $p["w_price_4"] + (!empty($p["w_price_{$i}"]) ? $p["w_price_{$i}"] : PRTNRgetCategoryWPrice($p['customerID'], $p['categoryID'], $i));
        $p['prices']['wp4'] = $p["w_price_4"];
      }
    }
  }
  return $p;
}


/**
 * @param $product
 * @param int $is_admin
 * @param int $is_manager
 * @param int $nw_price
 * @param int $partner
 * @return mixed
 */
function PRTNRgeneratePrice($product, $is_admin = 0, $is_manager = 0, $nw_price = 0, $partner = 0){

  if (!$is_admin) {

    if (empty($partner)) {
      if (isset($_SESSION['choose_p'])){
        $partner = PRTNRgetFromId(intval($_SESSION['choose_p']));
      }
      else {
        $partner = PRTNRget();
      }
    }

    if (!empty($partner)) {
      if (empty($nw_price))
        $nw_price = PRTNRgetNWPrice($partner, $product['categoryID']);

      $product['prices']['original'] = $product['Price'];

      $original_price = floatval($product['Price']);
      $w_price_4 = floatval($product["w_price_4"]);

      if ($nw_price > 0){
        $p_discount =  floatval($product["w_price_{$nw_price}"]);

        if ($nw_price > 0 && $nw_price < 5){
          if ($nw_price == 4) {
            if ($p_discount > 0)
              $product['Price'] = $p_discount;
          } else {
            if (!empty($w_price_4)){
              if ($p_discount > 0)
                $product['Price'] = $p_discount + $w_price_4;
              else {
                $c_discount = PRTNRgetCategoryWPrice($partner, $product['categoryID'], $nw_price);
                if ($c_discount > 0)
                  $product['Price'] = $c_discount + $w_price_4;
              }
            }
          }
        }
        //$product["PriceWithUnit"]	= show_price($product['Price']);

        if ($original_price != $product['Price'])
          $product['PriceOriginal'] = "Розница: ".show_price($original_price);
      }

      if ($is_manager) {
        $product['prices']['wp1'] = $product["w_price_4"] + (!empty($product["w_price_1"]) ? $product["w_price_1"] : PRTNRgetCategoryWPrice($partner, $product['categoryID'], 1));
        $product['prices']['wp2'] = $product["w_price_4"] + (!empty($product["w_price_2"]) ? $product["w_price_2"] : PRTNRgetCategoryWPrice($partner, $product['categoryID'], 2));
        $product['prices']['wp3'] = $product["w_price_4"] + (!empty($product["w_price_3"]) ? $product["w_price_3"] : PRTNRgetCategoryWPrice($partner, $product['categoryID'], 3));
        $product['prices']['wp4'] = $product["w_price_4"];
      }
    }
  }

  return $product;
}

/**
 * Your personal manager
 * @param int $mid
 * @return array
 */
function PRTNRyourManager($mid){
  if (empty($mid)) {
    $prtnr = PRTNRget();
    $mid = intval($prtnr['parent_partner']);
  }

  if (!empty($mid)){
    $q = db_query("SELECT * FROM ".CUSTOMERS_TABLE." WHERE customerID='{$mid}' && is_partner=1 && is_manager=1");
    $p = db_fetch_row($q);   
    
    if (!empty($p)){
      $p['Phone'] = PRTNRgetRegField(PRTNRgetRegFields($p['customerID']), 'Телефон');
      return $p;    
    }
  }else {
    return PRTNRgetManagerDefault();
  }
}

/**
 * Check if partner is actual
 * @return int
 */
function PRTNRcheck(){
  $prtnrs = PRTNRgets();
  $i = 0;
  $days_ago = mktime(0, 0, 0, date("m"),   date("d")-40,   date("Y"));
  foreach ($prtnrs as $prtnr) {
    $last_order = 0;
    $q = db_query("SELECT * FROM `SC_orders` WHERE customerID='{$prtnr['customerID']}' AND order_time IS NOT NULL AND statusID > 1 ORDER BY order_time LIMIT 0,1");
    $order = db_fetch_row($q);
    if ($order['order_time'])
      $last_order = strtotime($order['order_time']);

    if (!$prtnr['is_manager'])
      if (empty($last_order) || $last_order < $days_ago) {
        PRTNRunset($prtnr['customerID']);
        $i++;
      }
  }
  return $i;
}

/**
 * Unset is_partner
 * @param $pid
 */
function PRTNRunset($pid){
  $sql = "UPDATE ".CUSTOMERS_TABLE." SET is_partner = '0' WHERE customerID = {$pid}";
  db_query($sql);
}

/**
 * Send mails
 * @param $iid
 */
function PRTNRsendEmails($iid) {
  $smarty_mail = new View();
  $smarty_mail->template_dir = DIR_TPLS."/email";

  $prtnrs = PRTNRgets();
  $inflow = NFLWget($iid);
  $text = '';

  if ($inflow) {
    $date = NFLWgetDate($inflow['date']);
    $inflow_items = NFLWgetItems($iid);

    foreach ($prtnrs as $prtnr) {
      $manager = PRTNRyourManager($prtnr['parent_partner']);
      $prtnr =  array('is_manager' => $prtnr['is_manager'], 'w_price' => $prtnr['w_price'], 'customerID' => $prtnr['customerID'], 'Email' => $prtnr['Email']);

      $_inflow_items = array();
      foreach( $inflow_items as $i => $inflow_item ) {
        $inflow_item['product'] = _PRTNRgeneratePrice($inflow_item['product'], $prtnr, 0, $prtnr['w_price']);
        $_inflow_items[] = $inflow_item;
      };

      $inflow_items = $_inflow_items;

      $smarty_mail->assign( "mail", CONF_GENERAL_EMAIL );
      $smarty_mail->assign( "mmail", $manager['Email'] );
      $smarty_mail->assign( "mphone", $manager['Phone'] );
      $smarty_mail->assign( "inflow_items", $inflow_items );

      $html = $smarty_mail->fetch( "inflow_notification.txt" );

      ss_mail($prtnr['Email'], "Новое пополнение ({$date})", $html, true);
    }
  }
}

?>