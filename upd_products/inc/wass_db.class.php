<?php
class WASSDB{
	public static function connectDB(){
		$link = mysql_connect(DB_HOST, DB_USER, DB_PASS); 
		if(!$link){
			self::writeLog("Could not connect to DB: " . mysql_error(), 0);
			exit;
		}
		$db = mysql_select_db(DB_NAME);
		if(!$db){
			self::writeLog(mysql_error(), 0);
			exit;
		}
		 mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'"); 
		return true;
	}
	
	public static function db_query($query){
		$result = mysql_query($query);
		if(!$result){
			if((mysql_errno() == 2006)||(mysql_error() == "MySQL server has gone away")){
				//writeLog('Reconnect...'); 
				mysql_close();
				self::connectDB();
				return self::db_query($query);
			}
			self::writeLog('MySQL error #'.mysql_errno().' : '.mysql_error().'. Query: '.$query, 0);
		}
		return $result;
	}

	public static function clearAll()
	{
		//-----����� ������� ���� ���������
		$query_drop_enabled = 'UPDATE `'.DB_PREFIX.'products` SET enabled=0 WHERE `product_code` IS NOT NULL AND `product_code`<>""';
		$res_drop_enabled = self::db_query($query_drop_enabled);
		//------
	}
	
	public static function updateProduct($product, $upd_opts){
		$query = 'UPDATE `'.DB_PREFIX.'products` SET ';
		if($upd_opts['Price']){
			$query .= ' `Price`='.SV::Variable($product['price'],'trim:mysql:qt').',';
		}
		if($upd_opts['enabled']){
			$query .= ' `enabled`='.SV::Variable($product['available'],'intval:mysql:qt').',';
		}
		if($upd_opts['available']){
			$query .= ' `ordering_available`='.SV::Variable($product['available'],'intval:mysql:qt').',';
		}
		if($upd_opts['in_stock']){
			$in_stock = ($product['available'])?100:0;
			$query .= ' `in_stock`='.SV::Variable($in_stock,'intval:mysql:qt').',';
		}
		$query = substr($query, 0, -1);
		$query .= ' WHERE `product_code`='.SV::Variable($product['code'],'trim:mysql:qt');	
		$result = self::db_query($query);
		
		$m_info = mysql_info();
		preg_match('~matched\:\s([\d]+)\s+Changed\:\s([\d]+)~is',$m_info,$m);
		
		return array($m[1],$m[2]);;
	}
		
	public static function writeLog($msg = '', $status = 1){
		$status_str = ($status)? 'OK' : 'ERR';
		$log_str = sprintf("[%8s]  %-5s%s\r\n", date("H:i:s"), $status_str, $msg);
		error_log($log_str, 3, LOG_FILE);
	}
}
?>